package sessions.day18.thread.forkjoin;

import java.util.concurrent.CompletableFuture;
//https://drive.google.com/file/d/1TD-V6vTZynT24bZKoULEOMSOF1CnPsDg/view?usp=sharing
//https://www.youtube.com/watch?v=KR9NycfyvKg&list=PLZ9NgFYEMxp5NbV2NQ2moSX-V_Jv1aiGH&index=136&t=0s
//https://www.youtube.com/watch?v=dj1JwmUMTfg&list=PLZ9NgFYEMxp5NbV2NQ2moSX-V_Jv1aiGH&index=128
//https://www.youtube.com/watch?v=9ueIL0SwEWI
public class _05_CompletableFuture1 {

	public static void main(String[] args) throws InterruptedException {
		System.out.println("main: " + Thread.currentThread());
		CompletableFuture<Integer> cf = create();
		Thread.sleep(1000);
		cf.thenAccept(data -> printIt(data));
		Thread.sleep(1000);
		System.out.println("Another Way");

		create().thenAccept(data -> printIt(data));
		System.out.println("Print using "+Thread.currentThread());
		create().thenAcceptAsync(data -> printIt(data));
		System.out.println("Print using "+Thread.currentThread());
	}

	public static void printIt(int value) {
		System.out.println("PrintIt " + Thread.currentThread());
		System.out.println("value: " + value);
	}

	public static int compute() {
		System.out.println("Compute: " + Thread.currentThread());
		return 2;
	}

	public static CompletableFuture<Integer> create() {
		return CompletableFuture.supplyAsync(() -> compute());
	}
}
