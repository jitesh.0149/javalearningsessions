package sessions.day18.thread.forkjoin;


import java.util.concurrent.RecursiveTask;

/*
 * To Demonstrate the complete method of ForkJoinTask
 * As per Javadoc of complete:
 * 		Completes this task, and if not already aborted or cancelled,returning the given 
 * 		value as the result of subsequent invocations of join and related operations. 
 * 		This method may be used to provide results for asynchronous tasks, or to provide
 * 		alternative handling for tasks that would not otherwise complete normally. 
 * 		Its use in other situations is discouraged. This method is overridable, 
 * 		but overridden versions must invoke super implementation to maintain guarantees.
 */
public class _04_ForkJoinDemo4 {
	public static void main(String[] args) {
		RecursiveTaskEx task = new RecursiveTaskEx(2);
		System.out.println(task.invoke()); // If we use get() or join() here then program keeps running
		task.complete(4);
		System.out.println(task.invoke());
		task.complete(5);
		System.out.println(task.join());

	}
}

class RecursiveTaskEx extends RecursiveTask<Integer> {

	private static final long serialVersionUID = 1L;
	final int n;

	public RecursiveTaskEx(int n) {
		this.n = n;
	}

	@Override
	protected Integer compute() throws ArithmeticException {
		return n;
	}

}