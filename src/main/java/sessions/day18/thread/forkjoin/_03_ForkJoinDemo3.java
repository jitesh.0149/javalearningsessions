package sessions.day18.thread.forkjoin;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
//https://www.youtube.com/watch?v=5wgZYyvIVJk
import java.util.concurrent.RecursiveTask;
/*To demonstrate the difference between 
 * i)  ForkJoinTask's get(), join(), invoke()
 * ii) ForkJoinPool's submit(ForkJoinTask), invoke(ForkJoinTask)
 */
public class _03_ForkJoinDemo3 {
	public static void main(String[] args) {
		ForkJoinPool fjp = new ForkJoinPool();
		Fibonacci1 f = new Fibonacci1(6);
		// 0 1 1 2 3 5 8
		try {
			
			//Difference between join and get well explained at 
			// https://stackoverflow.com/questions/45490316/completablefuture-join-vs-get
			System.out.println(fjp.submit(f).get()); //if we use get we need to catch the checked exceptions
			//System.out.println(fjp.submit(f).join());
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}

		System.out.println("Program Completed ... ");
		// Alternatively can be returned as fjp.invoke(f)

		// Below is the source code
		/*  Difference between invoke and submit
		 public <T> T invoke(ForkJoinTask<T> task) {
		    if (task == null)
		        throw new NullPointerException();
		    externalPush(task);
		    return task.join();
		}
		
		 public <T> ForkJoinTask<T> submit(ForkJoinTask<T> task) {
		    if (task == null)
		        throw new NullPointerException();
		    externalPush(task);
		    return task;
		}
		*/

		/*
		  Difference between task.join vs task.get
		  join (JavaDoc) -  
				Returns the result of the computation when it is done. 
				This method differs from get() in that abnormal completion results in RuntimeException or Error, 
				not ExecutionException, and that interrupts of the calling thread do not cause the method
				to abruptly return by throwing 
		  
		  public final V get() throws InterruptedException, ExecutionException {
		        int s = (Thread.currentThread() instanceof ForkJoinWorkerThread) ?
		            doJoin() : externalInterruptibleAwaitDone();
		        Throwable ex;
		        if ((s &= DONE_MASK) == CANCELLED)
		            throw new CancellationException();
		        if (s == EXCEPTIONAL && (ex = getThrowableException()) != null)
		            throw new ExecutionException(ex);
		        return getRawResult();
		    }
		
		    public final V join() {
		        int s;
		        if ((s = doJoin() & DONE_MASK) != NORMAL)
		            reportException(s);
		        return getRawResult();
		    }
		 */
	}
}

class Fibonacci1 extends RecursiveTask<Integer> {

	private static final long serialVersionUID = 1L;
	final int n;

	public Fibonacci1(int n) {
		this.n = n;
	}

	@SuppressWarnings("unused")
	@Override
	protected Integer compute() throws ArithmeticException {
		System.out.println(Thread.currentThread());
		if (n <= 1)
			return n;

// 		To Check the exception
//		if (n > 1) {
//			int i = n / 0;
//		}
		// One way
		Fibonacci1 f1 = new Fibonacci1(n - 1);
		f1.fork();
		Fibonacci1 f2 = new Fibonacci1(n - 2);
		f2.fork();
		return f1.join() + f2.join();
		
		// Alternate way #1 single liner
		// https://stackoverflow.com/questions/17876144/fork-join-related-join-vs-get-vs-invoke
		// Method invoke() is semantically equivalent to fork(); join()
		// *** but always attempts to begin execution in the current thread ***
		// So if you were to call x.fork().join() it would be the same as x.invoke()
		// but the whole point is that you do work on the current Thread between invoking fork and join
		// Calling invoke waits for the invoked task to complete. So your method in now not asynchronous.

		// return new Fibonacci1(n-1).invoke() + new Fibonacci1(n-2).invoke();
		
	}

}