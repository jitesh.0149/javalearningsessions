package sessions.day18.thread.forkjoin;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;


//An example to work with to check what if we remove the sleep from different statemetns
public class _06_CompletableFuture2 {

	@SuppressWarnings("static-access")
	public static void main(String[] args) throws InterruptedException, ExecutionException {

		CompletableFuture
				.supplyAsync(
						() -> {
							System.out.println("supplyAsync 1: " + Thread.currentThread());
							try {
								Thread.sleep(3000);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
							return 1;
						})
				.thenApplyAsync((i) -> {
					System.out.println("thenApplyAsync 1: " + Thread.currentThread() + " i " + i);
					try {
						Thread.sleep(3000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

					return 1;
				})
				.handleAsync((i, j) -> {
					System.out.println("handleAsync 1: " + Thread.currentThread());
					System.out.println("i " + i + " j: " + j);
					try {
						Thread.sleep(3000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

					return 1;
				})
				.supplyAsync(
						() -> {
							System.out.println("supplyAsync 2: " + Thread.currentThread());
							try {
								Thread.sleep(3000);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}

							return 1;
						})
				.join();
	}
}
