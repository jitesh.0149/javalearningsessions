package sessions.day18.thread.forkjoin;
/*
 * The Main Fork/Join Classes
 * The Fork/Join Framework is packaged in java.util.concurrent. At the core of the Fork/Join Framework are the following four classes:
 * ForkJoinTask<V> - An abstract class that defines a task
 * ForkJoinPool - Manages the execution of ForkJoinTasks
 * RecursiveAction - A subclass of ForkJoinTask<V> for tasks that *** do not return values ***
 * RecursiveTask<V> - A subclass of ForkJoinTask<V> for tasks that *** return values ***
 */

/*
 * The execution of ForkJoinTasks takes place within a ForkJoinPool, which also manages the
 * execution of the tasks. Therefore, in order to execute a ForkJoinTask, you must first have a
 * ForkJoinPool. Beginning with JDK 8, there are two ways to acquire a ForkJoinPool. First,
 * you can explicitly create one by using a ForkJoinPool constructor. Second, you can use what
 * is referred to as the common pool. The common pool (which was added by JDK 8) is a static
 * ForkJoinPool that is automatically available for your use. Each method is introduced here,
 * beginning with manually constructing a pool.
 */

/*
 * There are two basic ways to start a task using the common pool. First, you can obtain a
 * reference to the pool by calling commonPool( ) and then use that reference to call invoke( )
 * or execute( ), as just described. Second, you can call ForkJoinTask methods such as fork( )
 * or invoke( ) on the task from outside its computational portion. In this case, the common
 * pool will automatically be used. In other words, fork( ) and invoke( ) will start a task using
 * the common pool if the task is not already running within a ForkJoinPool.
 * 
 * ForkJoinPool manages the execution of its threads using an approach called work-stealing.
 * Each worker thread maintains a queue of tasks. If one worker thread�s queue is empty, it
 * will take a task from another worker thread. This adds to overall efficiency and helps
 * maintain a balanced load. (Because of demands on CPU time by other processes in the system,
 * even two worker threads with identical tasks in their respective queues may not complete at
 * the same time.)
 * 
 * One other point: ForkJoinPool uses daemon threads. A daemon thread is automatically
 * terminated when all user threads have terminated. Thus, there is no need to explicitly shut
 * down a ForkJoinPool. However, with the exception of the common pool, it is possible to do
 * so by calling shutdown( ). The shutdown( ) method has no effect on the common pool.
 */
//A simple example of the basic divide-and-conquer strategy.
//In this case, RecursiveAction is used.
//https://dzone.com/articles/be-aware-of-forkjoinpoolcommonpool
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

class SqrtTransform extends RecursiveAction {
	private static final long serialVersionUID = -627250866624804994L;
	// The threshold value is arbitrarily set at 1,000 in this example.
	// In real-world code, its optimal value can be determined by
	// profiling and experimentation.
	final int seqThreshold = 1000;
	// Array to be accessed.
	double[] data;
	// Determines what part of data to process.
	int start, end;

	SqrtTransform(double[] vals, int s, int e) {
		data = vals;
		start = s;
		end = e;
	}

	// This is the method in which parallel computation will occur.
	@Override
	protected void compute() {
		// If number of elements is below the sequential threshold,
		// then process sequentially.
		if ((end - start) < seqThreshold) {
			// Transform each element into its square root.
			for (int i = start; i < end; i++) {
				data[i] = Math.sqrt(data[i]);
			}
		} else {
			System.out.println("Divide start: " + start + " end: " + end);
			// Otherwise, continue to break the data into smaller pieces.
			// Find the midpoint.
			int middle = (start + end) / 2;
			// Invoke new tasks, using the subdivided data.
			invokeAll(new SqrtTransform(data, start, middle),
					new SqrtTransform(data, middle, end));
		}
	}
}

//Demonstrate parallel execution.
class _01_ForkJoinDemo1 {
	public static void main(String args[]) {
		// Create a task pool.
		ForkJoinPool fjp = new ForkJoinPool();
		double[] nums = new double[100000];
		// Give nums some values.
		for (int i = 0; i < nums.length; i++)
			nums[i] = (double) i;
		System.out.println("A portion of the original sequence:");
		for (int i = 0; i < 10; i++)
			System.out.print(nums[i] + " ");
		System.out.println("\n");
		SqrtTransform task = new SqrtTransform(nums, 0, nums.length);
		// Start the main ForkJoinTask.
		
		fjp.invoke(task);  //will wait for result
		//alternatively we can write task.invoke()
		// another way is fjp.execute(task); which returns void and won't wait for result
		System.out.println("A portion of the transformed sequence" +
				" (to four decimal places):");
		for (int i = 0; i < 10; i++)
			System.out.format("%.4f ", nums[i]);
		System.out.println();
	}
}