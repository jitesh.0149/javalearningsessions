package sessions.day17.thread.executorservices;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class _04_CallableExample {

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		ExecutorService es = Executors.newFixedThreadPool(10);
		
		List<Future<Integer>> futures = new ArrayList<>();
		
		for (int i = 0; i < 10; i++) {
			futures.add(es.submit(new CallableImpl()));		
		}
		
		for (int i = 0; i < 10; i++) {
			System.out.println("The output for thread #" + i + " is " + futures.get(i).get());
		}
		es.shutdown();
	}

}

class CallableImpl implements Callable<Integer> {

	@Override
	public Integer call() throws Exception {
		int number = new Random().nextInt(10000);
		//System.out.println(Thread.currentThread().getName() + " is sleeping for " + number + " milliseconds");
		Thread.sleep(number);
		//System.out.println(Thread.currentThread().getName() + " is now available after " + number + " milliseconds !!");
		return number;
	}

}