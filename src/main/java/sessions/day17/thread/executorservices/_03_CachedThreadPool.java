package sessions.day17.thread.executorservices;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class _03_CachedThreadPool {
	public static void main(String[] args) {
		ExecutorService es = Executors.newCachedThreadPool();
		// Other Options are
		// es = Executors.newCachedThreadPool(ThreadFactory);
		// es = Executors.newSingleThreadExecutor()
		// es = Executors.newSingleThreadExecutor(ThreadFactory)
		// es = Executors.newWorkStealingPool()
		// es = Executors.newWorkStealingPool(int parallalism)
		// es = Executors.newScheduledThreadPool(corePoolSize)
		// es = Executors.newScheduledThreadPool(corePoolSize, ThreadFactory)
		for (int i = 0; i < 10; i++) {
			es.execute(new Runnable() {

				@Override
				public void run() {
					System.out.println(Thread.currentThread().getName() + " started ...");
					try {
						Thread.sleep(3000);
						System.out.println(Thread.currentThread().getName() + " completed !!!");
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			});
		}
		es.shutdown();
	}
}