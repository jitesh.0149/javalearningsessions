package sessions.day17.thread.executorservices;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

public class _02_FixedThreadPool2 {
	public static void main(String[] args) {
		ExecutorService es = Executors.newFixedThreadPool(2, new ThreadFactorySample("Thread Test"));
		for (int i = 0; i < 10; i++) {
			es.execute(new Runnable() {

				@Override
				public void run() {
					System.out.println(Thread.currentThread().getName() + " started ...");
					try {
						System.out.println("Priority: " + Thread.currentThread().getPriority());
						Thread.sleep(3000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			});
		}
		es.shutdown();
	}
}

class ThreadFactorySample implements ThreadFactory {
	private int threadsNum;
	private final String namePattern;

	public ThreadFactorySample(String baseName) {
		namePattern = baseName + "-%d";
	}

	@Override
	public Thread newThread(Runnable runnable) {
		System.out.println("In newThread method .....");
		threadsNum++;
		Thread t = new Thread(runnable, String.format(namePattern, threadsNum));
		t.setPriority(9);
		return t;
	}
}