package sessions.day17.thread.executorservices;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/*
 * There are three implementations of ExecutorService:
 * ThreadPoolExecutor, ScheduledThreadPoolExecutor, and ForkJoinPool
 */
public class _01_FixedThreadPool {
	public static void main(String[] args) throws InterruptedException {
		ExecutorService es = Executors.newFixedThreadPool(2);
		for (int i = 0; i < 10; i++) {
			es.execute(new Runnable() {

				@Override
				public void run() {
					System.out.println(Thread.currentThread().getName() + " started ...");
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			});
		}
		es.shutdown(); // If this statement is not there then program will not terminate
		es.awaitTermination(Long.MAX_VALUE, TimeUnit.MINUTES);
		// If this statement is not there then the following statement will be executed immidiately
		// even though the threads' task is not completed
		System.out.println("Other routines ...");
	}
}
