package sessions.day17.thread.executorservices;

import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

//For the AtomicInteger with simple threads and ThreadPool with different scenarios
public class _0N_ExecuterServicePerformance {
	public static void main(String[] args) throws InterruptedException {
		int numberOfThreads = 500;
		Thread[] threads = new Thread[numberOfThreads];

		Date begin = new Date();
		TaskAtomic taskAtomic = new TaskAtomic();
		for (int i = 0; i < numberOfThreads; i++) {
			threads[i] = new Thread(taskAtomic);
			threads[i].start();
		}
		for (int i = 0; i < numberOfThreads; i++) {
			threads[i].join();
		}
		System.out.println("Total time taken using AtomicInteger: " + ((new Date()).getTime() - begin.getTime()));

		// With exact number of thread pool
		begin = new Date();
		taskAtomic = new TaskAtomic();
		ExecutorService es = Executors.newFixedThreadPool(numberOfThreads);
		for (int i = 0; i < numberOfThreads; i++) {
			es.submit(taskAtomic);
		}
		es.shutdown();
		es.awaitTermination(Integer.MAX_VALUE, TimeUnit.SECONDS);
		System.out.println("Total time taken using AtomicInteger (using Executor Service - newFixedThreadPool): " + ((new Date()).getTime() - begin.getTime()));

		// With exact number of thread pool
		begin = new Date();
		taskAtomic = new TaskAtomic();
		es = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
		for (int i = 0; i < numberOfThreads; i++) {
			es.submit(taskAtomic);
		}
		es.shutdown();
		es.awaitTermination(Integer.MAX_VALUE, TimeUnit.SECONDS);
		System.out.println("Total time taken using AtomicInteger (using newFixedThreadPool and lower thread pool size): " + ((new Date()).getTime() - begin.getTime()));

		// With cached thread pool
		begin = new Date();
		taskAtomic = new TaskAtomic();
		es = Executors.newCachedThreadPool();
		for (int i = 0; i < numberOfThreads; i++) {
			es.submit(taskAtomic);
		}
		es.shutdown();
		es.awaitTermination(Integer.MAX_VALUE, TimeUnit.SECONDS);
		System.out.println("Total time taken using AtomicInteger (using Executor Service - CachedThreadPool): " + ((new Date()).getTime() - begin.getTime()));

		// With WorkAndStealPool
		begin = new Date();
		taskAtomic = new TaskAtomic();
		es = Executors.newWorkStealingPool();
		for (int i = 0; i < numberOfThreads; i++) {
			es.submit(taskAtomic);
		}
		es.shutdown();
		es.awaitTermination(Integer.MAX_VALUE, TimeUnit.SECONDS);
		System.out.println("Total time taken using AtomicInteger (using Executor Service - WorkAndStealPool): " + ((new Date()).getTime() - begin.getTime()));

		// With WorkAndStealPool and parallalism with limited threads
		begin = new Date();
		taskAtomic = new TaskAtomic();
		es = Executors.newWorkStealingPool(20);
		for (int i = 0; i < numberOfThreads; i++) {
			es.submit(taskAtomic);
		}
		es.shutdown();
		es.awaitTermination(Integer.MAX_VALUE, TimeUnit.SECONDS);
		System.out.println("Total time taken using AtomicInteger (using Executor Service - WorkAndStealPool and parallalism): " + ((new Date()).getTime() - begin.getTime()));

	}

}

class TaskAtomic implements Runnable {
	AtomicInteger number = new AtomicInteger(0);

	@Override
	public void run() {
		for (int i = 0; i < 1000000; i++) {
			number.set(i);
		}
	}
}