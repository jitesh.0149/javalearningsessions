package sessions.day16.lambda.functioninterfaces;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class _04_WithConsumer {

	static public List<Person> roster;

	public static void main(String[] args) {
		roster = new ArrayList<Person>();
		roster.add(new Person("User-1", Person.Sex.MALE, 26));
		roster.add(new Person("User-2", Person.Sex.MALE, 25));
		processPersons(
				roster,
				p -> p.getGender() == Person.Sex.MALE
						&& p.getAge() >= 18
						&& p.getAge() <= 25,
				p -> p.printPerson()
				);
		processPersons(
				roster,
				p -> p.getGender() == Person.Sex.MALE
						&& p.getAge() >= 25
						&& p.getAge() <= 26,
				p -> System.out.println("New Method call: "+p.getAge())
				);
	}

	public static void processPersons(
			List<Person> roster,
			Predicate<Person> tester,
			Consumer<Person> block) {
		for (Person p : roster) {
			if (tester.test(p)) {
				block.accept(p);
			}
		}
	}

}