package sessions.day16.lambda.functioninterfaces;

import java.util.function.Consumer;

// This is a functional interface and can therefore be used 
// as the assignment target for a lambda expression or method reference.
public class _01_ConsumerImplSample implements Consumer<Person> {

	@Override
	public void accept(Person t) {

	}

}
