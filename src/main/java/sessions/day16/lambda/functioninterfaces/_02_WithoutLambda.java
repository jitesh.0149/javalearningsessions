package sessions.day16.lambda.functioninterfaces;

import java.util.*;

import sessions.day19_20.streams.CheckPerson;

public class _02_WithoutLambda {

	static public List<Person> roster;

	public static void main(String[] args) {
		roster = new ArrayList<Person>();
		roster.add(new Person("User-1", Person.Sex.MALE, 26));
		roster.add(new Person("User-2", Person.Sex.MALE, 25));
		printPersons(roster,
				new CheckPerson() {
					public boolean test(Person p) {
						return p.getGender() == Person.Sex.MALE
								&& p.getAge() >= 18
								&& p.getAge() <= 25;
					}
				});
	}

	public static void printPersons(
			List<Person> roster, CheckPerson tester) {
		for (Person p : roster) {
			if (tester.test(p)) {
				p.printPerson();
			}
		}
	}

}
