package sessions.day16.lambda.functioninterfaces;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class _05_WithFunction {

	static public List<Person> roster;

	public static void main(String[] args) {
		roster = new ArrayList<Person>();
		roster.add(new Person("User-1", Person.Sex.MALE, 26));
		roster.add(new Person("User-2", Person.Sex.MALE, 25));
		processPersonsWithFunction(
				roster,
				p -> p.getGender() == Person.Sex.MALE
						&& p.getAge() >= 18
						&& p.getAge() <= 25,
				p -> p.getEmailAddress(),
				manish -> System.out.println(manish));

		// Another way

		processPersonsWithFunction(
				roster,
				new Predicate<Person>() {

					@Override
					public boolean test(Person p) {
						return p.getGender() == Person.Sex.MALE
								&& p.getAge() >= 18
								&& p.getAge() <= 25;
					}
				},
				new Function<Person, String>() {

					@Override
					public String apply(Person p) {
						return p.getEmailAddress();
					}
				},
				new Consumer<String>() {

					@Override
					public void accept(String t) {
						System.out.println(t);

					}
				});
		
		processPersonsWithFunction1(
				roster,
				p -> p.getGender() == Person.Sex.MALE
						&& p.getAge() >= 18
						&& p.getAge() <= 25,
				p -> p.getEmailAddress(),
				(manish, kunal) -> System.out.println(kunal.getGender() + ":" + manish));
		
		
		processPersonsWithFunction2(
				roster,
				p -> p.getGender() == Person.Sex.MALE
						&& p.getAge() >= 18
						&& p.getAge() <= 25,
				(p, s) -> Arrays.asList((new Integer[] {
						100 })),
				(manish, kunal) -> System.out.println(kunal.getGender() + ":" + manish));
		//BiFunction<Person, String, List<Integer>> mapper,
	}

	public static void processPersonsWithFunction(
			List<Person> roster,
			Predicate<Person> tester,
			Function<Person, String> mapper,
			Consumer<String> block) {
		for (Person p : roster) {
			if (tester.test(p)) {
				String data = mapper.apply(p);
				block.accept(data);
			}
		}
		
	}
	
	public static void processPersonsWithFunction1(
			List<Person> roster,
			Predicate<Person> tester,
			Function<Person, String> mapper,
			BiConsumer<String,Person> block) {
		for (Person p : roster) {
			if (tester.test(p)) {
				String data = mapper.apply(p);
				block.accept(data, p);
			}
		}
	}
	
	public static void processPersonsWithFunction2(
			List<Person> roster,
			Predicate<Person> tester,
			BiFunction<Person, String, List<Integer>> mapper,
			BiConsumer<String,Person> block) {
		for (Person p : roster) {
			if (tester.test(p)) {
				Integer data = mapper.apply(p,"Test").get(0);
				block.accept(data.toString(), p);
			}
		}
	}

}