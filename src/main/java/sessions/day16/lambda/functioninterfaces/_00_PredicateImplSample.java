package sessions.day16.lambda.functioninterfaces;

import java.util.function.Predicate;

// This is a functional interface and can therefore be used as the assignment 
// target for a lambda expression or method reference.
public class _00_PredicateImplSample implements Predicate<Person> {

	@Override
	public boolean test(Person t) {
		return t.age > 20;
	}

}
