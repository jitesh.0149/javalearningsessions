package sessions.day16.lambda.functioninterfaces;

import java.util.*;
import java.util.function.Predicate;

public class _06_WithPredicates {

	static public List<Person> roster;

	public static void main(String[] args) {
		roster = new ArrayList<Person>();
		roster.add(new Person("User-1", Person.Sex.MALE, 26));
		roster.add(new Person("User-2", Person.Sex.MALE, 25));
		printPersonsWithPredicate(roster,
				(Person p) -> p.getGender() == Person.Sex.MALE
						&& p.getAge() >= 18
						&& p.getAge() <= 25);
	}

	public static void printPersonsWithPredicate(List<Person> roster, Predicate<Person> tester) {
		for (Person p : roster) {
			if (tester.test(p)) {
				p.printPerson();
			}
		}
	}

}