package sessions.day16.lambda.functioninterfaces;

import java.util.function.BiFunction;
import java.util.function.BinaryOperator;

public class _07_WithBinaryOperator {

	public static void main(String[] args) {
		BiFunction<Integer, Integer, String> func = (x, y) -> "Output is : " + (x + y);
		System.out.println(func.apply(1, 2));
		BiFunction<Integer, Integer, Integer> func1 = (x, y) -> (x * y);
		System.out.println("Mulplication : " + func1.apply(3, 5));

		/*
		 * BinaryOperator is a specialization of
		 * {@link BiFunction} for the case where the operands and the result are all of the same type.
		 */
		// BinaryOperator<T> extends BiFunction<T,T,T>
		BinaryOperator<Integer> binaryOperator = (x, y) -> (x * y);
		System.out.println("Same multiplication using BiaryOperator: " + binaryOperator.apply(3, 5));
		BinaryOperator<Integer> minBy = BinaryOperator.minBy((x, y) -> y - x);
		System.out.println("minBy: " + minBy.apply(15, 12));

	}

}
