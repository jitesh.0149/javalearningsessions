package sessions.day16.lambda.functioninterfaces;

import java.util.ArrayList;
import java.util.List;

import sessions.day19_20.streams.CheckPerson;

public class Person {

	String name;
	Sex gender;
	String emailAddress;
	int age;

	public Person(String name, Sex sex, int age) {
		this.name = name;
		this.gender = sex;
		this.age = age;
	}

	public Person(String name, Sex sex, int age, String emailAddress) {
		this.name = name;
		this.gender = sex;
		this.age = age;
		this.emailAddress = emailAddress;
	}

	public Sex getGender() {
		return this.gender;
	}

	public int getAge() {
		return this.age;
	}

	public String getEmailAddress() {
		return this.emailAddress == null ? "jitesh.0149@gmail.com" : this.emailAddress;
	}

	public static void printPersons(List<Person> roster, CheckPerson tester) {
		for (Person p : roster) {
			if (tester.test(p)) {
				p.printPerson();
			}
		}
	}

	public void printPerson() {
		System.out.println("Age of " + this.name + " is " + this.age);
	}

	public enum Sex {
		MALE, FEMALE
	}

	public static ArrayList<Person> getDefautList() {
		ArrayList<Person> roster = new ArrayList<Person>();
		roster.add(new Person("User-1", Person.Sex.MALE, 26, "user1@test.com"));
		roster.add(new Person("User-2", Person.Sex.MALE, 25, "user2@test.com"));
		roster.add(new Person("User-3", Person.Sex.FEMALE, 27, "user3@test.com"));
		roster.add(new Person("User-4", Person.Sex.MALE, 25, "user4@test.com"));
		roster.add(new Person("User-5", Person.Sex.FEMALE, 26, "user5@test.com"));
		return roster;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", gender=" + gender + ", age=" + age + "]";
	}

}