package sessions.day08.hashmap;

import java.util.TreeMap;
import java.util.TreeSet;

public class TreeMapExample {

	public static void main(String[] args) {
		TreeMap<Integer, String> treeMap = new TreeMap<Integer, String>();
		treeMap.put(1, "Test");
		treeMap.put(2, "Test2");
		treeMap.put(30, "Test2");
		treeMap.put(5, "Test2");
		treeMap.put(10, "Test2");
		System.out.println("Ended ...");

		for (Integer a : treeMap.keySet()) {
			System.out.println(a);
		}

		TreeSet<Integer> treeSet = new TreeSet<Integer>();
		treeSet.add(1);
		treeSet.add(2);
		treeSet.add(30);
		treeSet.add(5);
		treeSet.add(10);
		System.out.println("Ended ...");

		for (Integer a : treeSet) {
			System.out.println(a);
		}
	}

}
