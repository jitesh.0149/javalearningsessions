package sessions.day08.hashmap;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SetExample {

	public static void main(String[] args) {

		Set<Integer> set = new HashSet<Integer>();
		set.add(17);
		set.add(2);
		set.add(1);
		set.add(1);
		set.add(1);
		set.add(16);
		for (Integer a : set) {
			System.out.println(a);
		}
		
		System.out.println("For List");
		
		
		List<Integer> list = new ArrayList<Integer>();
		list.add(1);
		list.add(2);
		list.add(1);
		list.add(1);
		addUniqueElement(list, 1);
		addUniqueElement(list, 10);
		for (Integer a : list) {
			System.out.println(a);
		}

	}

	public static void addUniqueElement(List<Integer> list, Integer noToAdd) {
		for (Integer a : list) {
			if (a.equals(noToAdd)) {
				return;
			}
		}
		list.add(noToAdd);
	}

}
