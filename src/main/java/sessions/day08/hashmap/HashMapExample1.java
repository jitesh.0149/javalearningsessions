package sessions.day08.hashmap;

import java.util.HashMap;

public class HashMapExample1 {

	public static void main(String[] args) {
		System.out.println((15 & 1));
		System.out.println((15 & 17));
		System.out.println((15 & 33));

		HashMap<Integer, String> hm = new HashMap<Integer, String>();
		hm.put(5, "Test");
		hm.put(1, "Test");
		hm.put(1, "Test1");
		hm.put(17, "Test1");

		hm.put(-17, "Test2");

		System.out.println(Integer.valueOf(-17).hashCode());

		
		hm.put(-5, "Test2");
		hm.remove(1);
		hm.put(2, "Test1");

		System.out.println(hm.entrySet());
		hm.put(16, "Test1");
		hm.put(17, "Test1");
		hm.put(18, "Test1");
		hm.put(9, "Test1");
	}

}
