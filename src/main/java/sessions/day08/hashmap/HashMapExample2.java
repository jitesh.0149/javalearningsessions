package sessions.day08.hashmap;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

public class HashMapExample2 {

	public static void main(String[] args) {

		HashMap<Integer, String> hm = new HashMap<Integer, String>();
		hm.put(1, "Test");
		hm.put(2, "Test22");
		
		Set<Entry<Integer, String>> set = hm.entrySet();
		for (Entry<Integer, String> pair : set) {
			System.out.println(pair.getKey());
			System.out.println(pair.getValue());
		}
		hm.putIfAbsent(1, "Jitesh");
		
		System.out.println("Printing keys ...");
		Set<Integer> keySet = hm.keySet();
		for (Integer key : keySet) {
			System.out.println(key);
		}
	}

}
