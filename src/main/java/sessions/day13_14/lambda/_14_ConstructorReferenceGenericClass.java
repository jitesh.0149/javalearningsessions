package sessions.day13_14.lambda;

// Demonstrate a constructor reference with a generic class.
// MyFuncGeneric1 is now a generic functional interface.
interface MyFuncGeneric1<T> {
	MyClass1<T> func(T n);
}

class MyClass1<T> {
	private T val;

	// A constructor that takes an argument.
	MyClass1(T v) {
		val = v;
	}

	// This is the default constructor.
	MyClass1() {
		val = null;
	}
	// ...

	T getVal() {
		return val;
	};
}

public class _14_ConstructorReferenceGenericClass {
	public static void main(String args[]) {
		// Create a reference to the MyClass<T> constructor.
		MyFuncGeneric1<Integer> myClassCons = MyClass1<Integer>::new;
		// Create an instance of MyClass<T> via that constructor reference.
		MyClass1<Integer> mc = myClassCons.func(100);
		// Use the instance of MyClass<T> just created.
		System.out.println("val in mc is " + mc.getVal());
	}
}