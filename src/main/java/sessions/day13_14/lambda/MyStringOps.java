package sessions.day13_14.lambda;

//This class defines a static method called strReverse().
public class MyStringOps {
	// A static method that reverses a string.
	public static String strReverse(String str) {
		String result = "";
		int i;
		for (i = str.length() - 1; i >= 0; i--)
			result += str.charAt(i);
		return result;
	}

	// A non-static method that reverses a string.
	public String strReverseNonStatic(String str) {
		String result = "";
		int i;
		for (i = str.length() - 1; i >= 0; i--)
			result += str.charAt(i);
		return result;
	}
}