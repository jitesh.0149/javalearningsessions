package sessions.day13_14.lambda;

// Demonstrate a Constructor reference.
// MyFunc is a functional interface whose method returns a MyClass reference.
interface MyFunc {
	MyClass13 func(int n);
}

class MyClass13 {
	private int val;

	// This constructor takes an argument.
	MyClass13(int v) {
		System.out.println("Test Inside the constroctor");
		val = v;
	}

	// This is the default constructor.
	MyClass13() {
		val = 0;
	}

	// ...
	int getVal() {
		return val;
	};
	     //MyClass13 func(int n);
	static MyClass13 test(int v) {
		return new MyClass13();
	}
	
	
}

public class _13_ConstructorReference {
	public static void main(String args[]) {
		// Create a reference to the MyClass constructor.
		// Because func() in MyFunc takes an argument, new refers to the parameterized
		// constructor in MyClass, not the default constructor.
		MyFunc myClassCons = MyClass13::new;
		// Create an instance of MyClass via that constructor reference.

		MyClass13 mc = myClassCons.func(100);
		// Use the instance of MyClass just created.
		System.out.println("val in mc is " + mc.getVal());
	}
}