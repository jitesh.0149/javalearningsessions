package sessions.day13_14.lambda;

/* 
It is also possible to handle a situation in which you want to specify an instance method
that can be used with any object of a given class�not just a specified object. In this case,
you will create a method reference as shown here:
ClassName::instanceMethodName
Here, the name of the class is used instead of a specific object, even though an instance
method is specified. With this form, the first parameter of the functional interface matches
the invoking object and the second parameter matches the parameter specified by the
method. Here is an example. It defines a method called counter( ) that counts the number
of objects in an array that satisfy the condition defined by the func( ) method of the MyFunc
functional interface. In this case, it counts instances of the HighTemp class.

 */

// A class that stores the temperature high for a day.
class HighTemp {
	private int hTemp;

	HighTemp(int ht) {
		hTemp = ht;
	}

	// Return true if the invoking HighTemp object has the same temperature as ht2.
	boolean sameTemp(HighTemp ht2) {
		return hTemp == ht2.hTemp;
	}

	// Return true if the invoking HighTemp object has a temperature that is less than ht2.
	boolean lessThanTemp(HighTemp ht2) {
		return hTemp < ht2.hTemp;
	}
}

public class _10_MethodReferenceInstanceMethodOtherObject {
	// A method that returns the number of occurrences of an object for which some criteria, as specified by the MyFunc parameter, is true.
	static <T> int counter(T[] vals, MyFuncBoolean<T> f, T v) {
		int count = 0;
		for (int i = 0; i < vals.length; i++)
			if (f.func(vals[i], v))
				count++;
		return count;
	}

	public static void main(String args[]) {
		int count;
		// Create an array of HighTemp objects.
		HighTemp[] weekDayHighs = { new HighTemp(89), new HighTemp(82),
				new HighTemp(90), new HighTemp(89),
				new HighTemp(89), new HighTemp(91),
				new HighTemp(84), new HighTemp(83) };

		// Use counter() with arrays of the class HighTemp.
		// Notice that a reference to the instance method sameTemp() is passed as the second argument.
		count = counter(weekDayHighs, HighTemp::sameTemp, new HighTemp(89));
		System.out.println(count + " days had a high of 89");

		// Now, create and use another array of HighTemp objects.
		HighTemp[] weekDayHighs2 = { new HighTemp(32), new HighTemp(12),
				new HighTemp(24), new HighTemp(19),
				new HighTemp(18), new HighTemp(12),
				new HighTemp(-1), new HighTemp(13) };
		count = counter(weekDayHighs2, HighTemp::sameTemp,
				new HighTemp(12));
		System.out.println(count + " days had a high of 12");

		// Now, use lessThanTemp() to find days when temperature was less than a specified value.
		count = counter(weekDayHighs, HighTemp::lessThanTemp,
				new HighTemp(89));
		System.out.println(count + " days had a high less than 89");
		count = counter(weekDayHighs2, HighTemp::lessThanTemp,
				new HighTemp(19));
		System.out.println(count + " days had a high of less than 19");
	}
}