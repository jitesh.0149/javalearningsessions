package sessions.day13_14.lambda;

/* https://stackoverflow.com/questions/24294846/performance-difference-between-java-8-lambdas-and-anonymous-inner-classes
 * Oracle has posted a study comparing performance between Lambdas and anonymous classes  
 * See JDK 8: Lambda Performance Study by Sergey Kuksenko, which is 74 slides long.
 * Summary: slow to warm up but when JIT inlines it worst case just as fast as anonymous class but can be faster
 * https://stackoverflow.com/questions/34585444/java-lambdas-20-times-slower-than-anonymous-classes
 */
public class _01_BasicExample {
	public static void main(String[] args) {
		// Traditional Way Without Lambda
		MyInterface myInterfaceImpl1 = new MyInterfaceImpl();
		System.out.println(myInterfaceImpl1.add(1, 3));

		// Traditional Way Without Lambda (Anonymous Class)
		MyInterface myInterfaceImpl2 = new MyInterface() {
			@Override
			public int add(int a, int b) {
				return a + b;
			}
		};
		System.out.println(myInterfaceImpl2.add(1, 3));
		System.out.println(myInterfaceImpl2.add(3, 7));

		// Using Lambda
		MyInterface myInterface3 = (a, b) -> a + b;
		System.out.println(myInterface3.add(1, 3));
		System.out.println(myInterface3.add(3, 7));
	}

}

class MyInterfaceImpl implements MyInterface {

	@Override
	public int add(int a, int b) {
		return a + b;
	}

}
