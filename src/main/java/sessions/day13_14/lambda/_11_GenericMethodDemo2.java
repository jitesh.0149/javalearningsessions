package sessions.day13_14.lambda;

@FunctionalInterface
interface DriveTheVehicle {
	void drive();
}

public class _11_GenericMethodDemo2 {
	public static void driveACar() {
		System.out.println("I can drive by simply pressing the acce. ");
	}

	public static void driveACycle() {
		System.out.println("I need to balance during riding");
	}

	public static void main(String args[]) {
		_11_GenericMethodDemo2.driveACar();
		drive(_11_GenericMethodDemo2::driveACar);
		drive(_11_GenericMethodDemo2::driveACycle);
	}

	public static void drive(DriveTheVehicle dv) {
		System.out.println("Logging the information into dabase");
		dv.drive();
		System.out.println("Again updating something");
	}
}