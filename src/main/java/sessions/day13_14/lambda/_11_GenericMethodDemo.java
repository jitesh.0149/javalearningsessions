package sessions.day13_14.lambda;

// Demonstrate a method reference to a generic method declared inside a non-generic class.
// A functional interface that operates on an array and a value, and returns an int result.
interface MyFuncGeneric<T> {
	int func(T[] vals, T v);
}

// This class defines a method called countMatching() that returns the number of items in an array that are equal
// to a specified value. Notice that countMatching() is generic, but MyArrayOps is not.
class MyArrayOps<T> {
	static <Q> int countMatching(Q[] vals, Q v) {
		int count = 0;
		for (int i = 0; i < vals.length; i++)
			if (vals[i] == v)
				count++;
		return count;
	}

	<T> int countMatching1(T[] vals, T v) {
		int count = 0;
		for (int i = 0; i < vals.length; i++)
			if (vals[i] == v)
				count++;
		return count;
	}
}

public class _11_GenericMethodDemo {
	// This method has the MyFuncGeneric functional interface as the type of its
	// first parameter. The other two parameters
	// receive an array and a value, both of type T.
	static <T> int myOp(MyFuncGeneric<T> f, T[] vals, T v) {		
		return f.func(vals, v);
	}

	public static void main(String args[]) {
		Integer[] vals = { 1, 2, 3, 4, 2, 3, 4, 4, 5 };
		String[] strs = { "One", "Two", "Three", "Two" };
		int count;

		count = myOp(new MyFuncGeneric<Integer>() {

			@Override
			public int func(Integer[] vals, Integer v) {
				return 0;
			}

		}, vals, 4);

		
		//***************
		MyFuncGeneric<Integer> myf = MyArrayOps::countMatching;		
		myf.func(vals, 4);		
		MyArrayOps.countMatching(vals, 4);
		//***************		
		
		count = myOp(MyArrayOps::<Integer>countMatching, vals, 4);
		System.out.println("vals contains (Test) " + count + " 4s");
		count = myOp(MyArrayOps::countMatching, vals, 4);
		System.out.println("vals contains " + count + " 4s");
		count = myOp(MyArrayOps::<String>countMatching, strs, "Two");
		System.out.println("strs contains " + count + " Twos");

		MyArrayOps<Integer> a = new MyArrayOps<Integer>();
		count = myOp(a::countMatching1, vals, 4);
		System.out.println("vals contains " + count + " 4s");
	}
}