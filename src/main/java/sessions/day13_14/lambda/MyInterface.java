package sessions.day13_14.lambda;

// Below annotation is mandatory but if it is there then it guarantees that
// the interface is functional (i.e. has only one abstract method)
// @FunctionalInterface

public interface MyInterface {
	public int add(int a, int b);
}
