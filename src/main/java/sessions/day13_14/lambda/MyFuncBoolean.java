package sessions.day13_14.lambda;

//An example of capturing a local variable from the enclosing scope.
public interface MyFuncBoolean<T> {
	boolean func(T v1, T v2);
}