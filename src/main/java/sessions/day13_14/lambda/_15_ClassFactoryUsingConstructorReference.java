package sessions.day13_14.lambda;

// Implement a simple class factory using a constructor reference.
interface MyFuncGeneric2<R, T> {
	R func(T n);
}

// A simple generic class.
class MyClass2<T> {
	private T val;

	// A constructor that takes an argument.
	MyClass2(T v) {
		val = v;
	}

	// The default constructor. This constructor
	// is NOT used by this program.
	MyClass2() {
		val = null;
	}

	// ...
	T getVal() {
		return val;
	};
}

// A simple, non-generic class.
class MyClass22 {
	String str;

	// A constructor that takes an argument.
	MyClass22(String s) {
		str = s;
	}

	// The default constructor. This constructor is NOT used by this program.
	MyClass22() {
		str = "";
	}

	// ...
	String getVal() {
		return str;
	};
}

public class _15_ClassFactoryUsingConstructorReference {
	// A factory method for class objects. The class must have a constructor that takes one parameter of type T.
	// R specifies the type of object being created.
	static <R, T> R MyClass2Factory(MyFuncGeneric2<R, T> cons, T v) {
		return cons.func(v);
	}

	public static void main(String args[]) {
		// Create a reference to a MyClass2 constructor.
		// In this case, new refers to the constructor that takes an argument.
		MyFuncGeneric2<MyClass2<Double>, Double> MyClass2Cons = MyClass2<Double>::new;
		// Create an instance of MyClass2 by use of the factory method.
		MyClass2<Double> mc = MyClass2Factory(MyClass2Cons, 100.1);
		// Use the instance of MyClass2 just created.
		System.out.println("val in mc is " + mc.getVal());
		// Now, create a different class by use of MyClass2Factory().
		MyFuncGeneric2<MyClass22, String> MyClass2Cons2 = MyClass22::new;
		// Create an instance of MyClass22 by use of the factory method.
		MyClass22 mc2 = MyClass2Factory(MyClass2Cons2, "Lambda");
		// Use the instance of MyClass2 just created.
		System.out.println("str in mc2 is " + mc2.getVal());
	}
}