package sessions.day13_14.lambda;

//An example of capturing a local variable from the enclosing scope.
interface MyFuncInt {
	int func(int n);
}
