package sessions.day17_self.thread.atomic;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class _01_RaceConditionExample {

	public static void main(String[] args) throws InterruptedException {
		ExecutorService executorService = Executors.newFixedThreadPool(10);

		Counter counter = new Counter();

		for (int i = 0; i < 1000; i++) {
			executorService.submit(() -> counter.increment());
		}

		executorService.shutdown();
		executorService.awaitTermination(60, TimeUnit.SECONDS);

		executorService = Executors.newFixedThreadPool(10);
		System.out.println("Final count is : " + counter.getCount());
		
		
		AtomicCounter atomicCounter = new AtomicCounter();

		for (int i = 0; i < 1000; i++) {
			executorService.submit(() -> atomicCounter.increment());
		}

		executorService.shutdown();
		executorService.awaitTermination(60, TimeUnit.SECONDS);
		System.out.println("Final count using Atomic is : " + atomicCounter.getCount());
	}
}

class Counter {
	int count = 0;

	public void increment() {
		count = count + 1;
	}

	public int getCount() {
		return count;
	}
}

class AtomicCounter {
	AtomicInteger count = new AtomicInteger(0);

	public void increment() {
		count.incrementAndGet();
	}

	public int getCount() {
		return count.get();
	}
}