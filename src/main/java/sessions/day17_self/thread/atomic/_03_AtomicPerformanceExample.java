package sessions.day17_self.thread.atomic;

import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

//https://www.youtube.com/watch?v=-XipPj3tUu0
//https://stackoverflow.com/questions/281132/java-volatile-reference-vs-atomicreference
public class _03_AtomicPerformanceExample {
	public static void main(String[] args) throws InterruptedException {
		int numberOfThreads = 50;
		Thread[] threads = new Thread[numberOfThreads];

		Date begin = new Date();
		TaskLock taskLock = new TaskLock();
		for (int i = 0; i < numberOfThreads; i++) {
			threads[i] = new Thread(taskLock);
			threads[i].start();
		}
		for (int i = 0; i < numberOfThreads; i++) {
			threads[i].join();
		}
		System.out.println("Total time taken using lock: " + ((new Date()).getTime() - begin.getTime()));

		begin = new Date();
		TaskAtomic taskAtomic = new TaskAtomic();
		for (int i = 0; i < numberOfThreads; i++) {
			threads[i] = new Thread(taskAtomic);
			threads[i].start();
		}
		for (int i = 0; i < numberOfThreads; i++) {
			threads[i].join();
		}
		System.out.println("Total time taken using AtomicInteger: " + ((new Date()).getTime() - begin.getTime()));

		begin = new Date();
		TaskAtomicReference taskAtomicReference = new TaskAtomicReference();
		for (int i = 0; i < numberOfThreads; i++) {
			threads[i] = new Thread(taskAtomicReference);
			threads[i].start();
		}
		for (int i = 0; i < numberOfThreads; i++) {
			threads[i].join();
		}
		System.out.println("Total time taken using AtomicReference: " + ((new Date()).getTime() - begin.getTime()));

		// Below ExecutorService is covered in executorservice package
		begin = new Date();
		taskAtomic = new TaskAtomic();
		ExecutorService es = Executors.newFixedThreadPool(numberOfThreads);
		for (int i = 0; i < numberOfThreads; i++) {
			es.submit(new Thread(taskAtomic));
		}
		es.shutdown();
		es.awaitTermination(Integer.MAX_VALUE, TimeUnit.SECONDS);
		System.out.println("Total time taken using AtomicInteger (using Executor Service): " + ((new Date()).getTime() - begin.getTime()));

	}
}

class TaskLock implements Runnable {
	int number;
	Lock lock;

	public TaskLock() {
		super();
		lock = new ReentrantLock();

	}

	@Override
	public void run() {
		for (int i = 0; i < 1000000; i++) {
			lock.lock();
			number = i;
			lock.unlock();
		}
	}
}

class TaskAtomic implements Runnable {
	AtomicInteger number = new AtomicInteger(0);

	@Override
	public void run() {
		for (int i = 0; i < 1000000; i++) {
			number.set(i);
		}
	}
}

class TaskAtomicReference implements Runnable {
	AtomicReference<Integer> number = new AtomicReference<Integer>(0);

	@Override
	public void run() {
		for (int i = 0; i < 1000000; i++) {
			number.set(i);
		}
	}
}
