package sessions.day17_self.thread.atomic;

//http://tutorials.jenkov.com/java-util-concurrent/java-fork-and-join-forkjoinpool.html
//https://www.youtube.com/watch?v=5wgZYyvIVJk
import java.util.concurrent.RecursiveTask;

/*
 * https://stackoverflow.com/questions/50817843/difference-between-recursive-task-and-recursive-action-in-forkjoinpool
 * [RecursiveTask is] A recursive result-bearing ForkJoinTask.
 * [RecursiveAction is] A recursive resultless ForkJoinTask.
 */
public class _06_ForkJoinDemo2 {
	public static void main(String[] args) {
		Fibonacci f = new Fibonacci(5);
		// 0 1 1 2 3 5
		System.out.println("The output is : " + f.invoke());
	}
}

class Fibonacci extends RecursiveTask<Integer> {

	private static final long serialVersionUID = 1L;
	final int n;

	public Fibonacci(int n) {
		this.n = n;
	}

	@Override
	protected Integer compute() {
		if (n <= 1)
			return n;
		Fibonacci f1 = new Fibonacci(n - 1);
		f1.fork();
		Fibonacci f2 = new Fibonacci(n - 2);
		f2.fork();
		return f1.join() + f2.join();
	}

}