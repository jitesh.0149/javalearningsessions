package sessions.day17_self.thread.atomic;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.IntStream;

public class _02_AtomicReferenceExample {
	public static void main(String[] args) throws InterruptedException {
		final SampleClass CS1 = new SampleClass();
		ExecutorService es = Executors.newFixedThreadPool(20);
		IntStream.range(0, 10000).forEach((i) -> {
			es.execute(() -> CS1.value++);
		});
		es.shutdown();
		System.out.println(CS1.value);

		final AtomicReference<SampleClass> CS2 = new AtomicReference<>(new SampleClass());

		ExecutorService es1 = Executors.newFixedThreadPool(20);
		IntStream.range(0, 10000).forEach((i) -> {
			es1.execute(() -> {
				CS2.get().value++;
			});
		});
		es1.shutdown();
		es1.awaitTermination(60, TimeUnit.SECONDS);
		System.out.println(CS2.get().value);
		
		final AtomicReference<SampleClass> CS3 = new AtomicReference<>(new SampleClass());

		ExecutorService es2 = Executors.newFixedThreadPool(20);
		IntStream.range(0, 10000).forEach((i) -> {
			es2.execute(() -> {
				CS3.set(new SampleClass(i));
			});
		});
		es2.shutdown();
		es2.awaitTermination(60, TimeUnit.SECONDS);
		System.out.println(CS3.get().value);

	}
}

class SampleClass {
	int value;

	public SampleClass() {
	}

	public SampleClass(int value) {
		this.value = value;
	}
}