package sessions.day17_self.thread.utilities;

// An example of CountDownLatch.
import java.util.concurrent.CountDownLatch;

/*
 * Sometimes you will want a thread to wait until one or more events have occurred. To handle
 * such a situation, the concurrent API supplies CountDownLatch. A CountDownLatch is
 * initially created with a count of the number of events that must occur before the latch is
 * released. Each time an event happens, the count is decremented. When the count reaches
 * zero, the latch opens
 */
class _01_CountDownLatchDemo {
	public static void main(String args[]) {
		CountDownLatch cdl = new CountDownLatch(5);
		System.out.println("Starting");
		new MyThread(cdl);
		try {
			cdl.await();
		} catch (InterruptedException exc) {
			System.out.println(exc);
		}
		System.out.println("Done");
	}
}

class MyThread implements Runnable {
	CountDownLatch latch;

	MyThread(CountDownLatch c) {
		latch = c;
		new Thread(this).start();
	}

	public void run() {
		for (int i = 0; i < 5; i++) {
			System.out.println(i);
			latch.countDown(); // decrement count
		}
	}
}