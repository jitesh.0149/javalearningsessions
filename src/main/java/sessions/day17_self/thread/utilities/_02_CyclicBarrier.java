package sessions.day17_self.thread.utilities;

/*
 * A situation not uncommon in concurrent programming occurs when a set of two or more
 * threads must wait at a predetermined execution point until all threads in the set have
 * reached that point. To handle such a situation, the concurrent API supplies the CyclicBarrier
 * class. It enables you to define a synchronization object that suspends until the specified
 * number of threads has reached the barrier point.
  */
// An example of CyclicBarrier.
import java.util.concurrent.*;

class _02_CyclicBarrier {
	public static void main(String args[]) {
		CyclicBarrier cb = new CyclicBarrier(3, new BarAction());
		System.out.println("Starting");
		new MyTread1(cb, "A");
		new MyTread1(cb, "B");
		new MyTread1(cb, "C");
		new MyTread1(cb, "X");
		new MyTread1(cb, "Y");
		new MyTread1(cb, "Z");
	}
}

// A thread of execution that uses a CyclicBarrier.
class MyTread1 implements Runnable {
	CyclicBarrier cbar;
	String name;

	MyTread1(CyclicBarrier c, String n) {
		cbar = c;
		name = n;
		new Thread(this).start();
	}

	public void run() {
		System.out.println(name);
		try {
			cbar.await();
		} catch (BrokenBarrierException exc) {
			System.out.println(exc);
		} catch (InterruptedException exc) {
			System.out.println(exc);
		}
	}
}

// An object of this class is called when the
// CyclicBarrier ends.
class BarAction implements Runnable {
	public void run() {
		System.out.println("Barrier Reached!");
	}
}