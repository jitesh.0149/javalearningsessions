package sessions.day17_self.thread.utilities;

public class _06_CollectionUtilities {
	/*
	 * As explained, the concurrent API defines several collection classes that have been
	 * engineered for concurrent operation. They include:
	 * ArrayBlockingQueue
	 * ConcurrentHashMap
	 * ConcurrentLinkedDeque
	 * ConcurrentLinkedQueue
	 * ConcurrentSkipListMap
	 * ConcurrentSkipListSet
	 * CopyOnWriteArrayList
	 * CopyOnWriteArraySet
	 * DelayQueue
	 * LinkedBlockingDeque
	 * LinkedBlockingQueue
	 * LinkedTransferQueue
	 * PriorityBlockingQueue
	 * SynchronousQueue
	 */
}
