package sessions.day17_self.thread.utilities;

import java.util.concurrent.Phaser;

/*
 * Another synchronization class is called Phaser. Its primary purpose is to enable the
 * synchronization of threads that represent one or more phases of activity. For example, you
 * might have a set of threads that implement three phases of an order-processing application.
 * In the first phase, separate threads are used to validate customer information, check
 * inventory, and confirm pricing. When that phase is complete, the second phase has two
 * threads that compute shipping costs and all applicable tax. After that, a final phase confirms
 * payment and determines estimated shipping time. In the past, to synchronize the multiple
 * threads that comprise this scenario would require a bit of work on your part. With the
 * inclusion of Phaser, the process is now much easier.
 * To begin, it helps to know that a Phaser works a bit like a CyclicBarrier, described
 * earlier, except that it supports multiple phases. As a result, Phaser lets you define a
 * synchronization object that waits until a specific phase has completed. It then advances
 * to the next phase, again waiting until that phase concludes. It is important to understand
 * that Phaser can also be used to synchronize only a single phase. In this regard, it acts much
 * like a CyclicBarrier. However, its primary use is to synchronize multiple phases.
 */
class _04_PhaserDemo {
	public static void main(String args[]) {
		Phaser phsr = new Phaser(1);
		int curPhase;
		System.out.println("Starting");
		new MyThread2(phsr, "A");
		new MyThread2(phsr, "B");
		new MyThread2(phsr, "C");
		// Wait for all threads to complete phase one.
		curPhase = phsr.getPhase();
		phsr.arriveAndAwaitAdvance();
		System.out.println("Phase " + curPhase + " Complete");
		// Wait for all threads to complete phase two.
		curPhase = phsr.getPhase();
		phsr.arriveAndAwaitAdvance();
		System.out.println("Phase " + curPhase + " Complete");
		curPhase = phsr.getPhase();
		phsr.arriveAndAwaitAdvance();
		System.out.println("Phase " + curPhase + " Complete");

		// Deregister the main thread.
		phsr.arriveAndDeregister();
		if (phsr.isTerminated())
			System.out.println("The Phaser is terminated");
	}
}

// A thread of execution that uses a Phaser.
class MyThread2 implements Runnable {
	Phaser phsr;
	String name;

	MyThread2(Phaser p, String n) {
		phsr = p;
		name = n;
		phsr.register();
		new Thread(this).start();
	}

	public void run() {
		System.out.println("Thread " + name + " Beginning Phase One");
		phsr.arriveAndAwaitAdvance(); // Signal arrival.
		// Pause a bit to prevent jumbled output. This is for illustration
		// only. It is not required for the proper operation of the phaser.
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			System.out.println(e);
		}
		System.out.println("Thread " + name + " Beginning Phase Two");
		phsr.arriveAndAwaitAdvance(); 
		// Signal arrival.
		// Pause a bit to prevent jumbled output. This is for illustration
		// only. It is not required for the proper operation of the phaser.
		try {
			Thread.sleep(10);
		} catch (InterruptedException e) {
			System.out.println(e);
		}
		System.out.println("Thread " + name + " Beginning Phase Three");
		phsr.arriveAndDeregister(); // Signal arrival and deregister.
	}
}