package sessions.day17_self.thread.utilities;

/*
 * The synchronization object that many readers will immediately recognize is Semaphore,
 * which implements a classic semaphore. A semaphore controls access to a shared resource
 * through the use of a counter. If the counter is greater than zero, then access is allowed. If
 * it is zero, then access is denied. What the counter is counting are permits that allow access to
 * the shared resource. Thus, to access the resource, a thread must be granted a permit from
 * the semaphore.
 */

// A simple semaphore example.
import java.util.concurrent.*;

class _05_SemaphoreDemo {

	public static void main(String args[]) {
		Semaphore sem = new Semaphore(1);
		new IncThread(sem, "A");
		new DecThread(sem, "B");
	}
}

// A shared resource.
class Shared {
	static int count = 0;
}

// A thread of execution that increments count.
class IncThread implements Runnable {
	String name;
	Semaphore sem;

	IncThread(Semaphore s, String n) {
		sem = s;
		name = n;
		new Thread(this).start();
	}

	public void run() {
		System.out.println("Starting " + name);
		try {
			// First, get a permit.
			System.out.println(name + " is waiting for a permit.");
			sem.acquire();
			System.out.println(name + " gets a permit.");
			// Now, access shared resource.
			for (int i = 0; i < 5; i++) {
				Shared.count++;
				System.out.println(name + ": " + Shared.count);
				// Now, allow a context switch -- if possible.
				Thread.sleep(10);
			}
		} catch (InterruptedException exc) {
			System.out.println(exc);
		}
		// Release the permit.
		System.out.println(name + " releases the permit.");
		sem.release();
	}
}

// A thread of execution that decrements count.
class DecThread implements Runnable {
	String name;
	Semaphore sem;

	DecThread(Semaphore s, String n) {
		sem = s;
		name = n;
		new Thread(this).start();
	}

	public void run() {
		System.out.println("Starting " + name);
		try {
			// First, get a permit.
			System.out.println(name + " is waiting for a permit.");
			sem.acquire();
			System.out.println(name + " gets a permit.");
			// Now, access shared resource.
			for (int i = 0; i < 5; i++) {
				Shared.count--;
				System.out.println(name + ": " + Shared.count);
				// Now, allow a context switch -- if possible.
				Thread.sleep(10);
			}
		} catch (InterruptedException exc) {
			System.out.println(exc);
		}
		// Release the permit.
		System.out.println(name + " releases the permit.");
		sem.release();
	}
}