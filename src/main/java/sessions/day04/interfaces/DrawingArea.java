package sessions.day04.interfaces;

public class DrawingArea {
	String name;

	public void prepareABlankArea() {
		this.name = "drawingArea_" + Math.random();
		System.out.println("Preparing a blank area");
	}

}
