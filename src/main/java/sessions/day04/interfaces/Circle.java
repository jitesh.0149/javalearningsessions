package sessions.day04.interfaces;

public class Circle implements Shape {

	@Override
	public void draw() {
		DrawingArea d = Shape.prepareABlankArea();
		//Actually we need to draw a shape
		System.out.println("Drawing Circle inside " + d.name);

	}

	@Override
	public void erase() {
		System.out.println("Erasing Circle");

	}

}
