package sessions.day04.interfaces;

public class InterfaceDemo {
	public static void main(String[] args) {
		Circle s = new Circle();
		s.draw();
		Circle s1 = new Circle();
		s.draw();
		s1.sampleDefaultMethod();
	}
}
