package sessions.day04.interfaces;

public interface Shape {
	
	
	public void draw();

	public void erase();

	
	
	public default void sampleDefaultMethod() {
		//
		//
		System.out.println("Sample default method");
	}

	public static DrawingArea prepareABlankArea() {
		DrawingArea a = new DrawingArea();

		a.prepareABlankArea();
		return a;
		///
		//
		//
		///
	}

}
