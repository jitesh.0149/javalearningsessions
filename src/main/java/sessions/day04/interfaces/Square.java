package sessions.day04.interfaces;

public class Square implements Shape {

	@Override
	public void draw() {
		DrawingArea d = Shape.prepareABlankArea();
		
		System.out.println("Drawing Square inside " + d.name);

	}

	@Override
	public void erase() {
		System.out.println("Erasing Square");

	}

}
