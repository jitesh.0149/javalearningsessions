package sessions.day04.trycatch1;

public class OnlineShopping {

	public static String getItemDetail(int itemNumber) throws Exception {

		// try to fetch detail from database

		if (itemNumber > 100) {
			//itemNumber = itemNumber / 0;
			throw new RuntimeException("A runtimeException");
		}

		if (itemNumber == 5) {
			throw new Exception("Never ask for itemNumber 5");
		}

		return "Some Description for item #" + itemNumber;
	}
}
