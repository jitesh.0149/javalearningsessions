package sessions.day04.trycatch1;

public class TryCatchExample {

	public static void main(String[] args) {

		try {
			//read from db (i.e. connection open)
			System.out.println(OnlineShopping.getItemDetail(2));
			// System.out.println(OnlineShopping.getItemDetail(102));

			try {
				System.out.println(OnlineShopping.getItemDetail(5));
			} catch (Exception e) {
				// Add the error into message list
				System.out.println(e.getMessage());
			}
			System.out.println(OnlineShopping.getItemDetail(2));
			System.out.println(OnlineShopping.getItemDetail(7));
			System.out.println(OnlineShopping.getItemDetail(8));
			System.out.println(OnlineShopping.getItemDetail(10));
			// Exception
			System.out.println(OnlineShopping.getItemDetail(5));

			System.out.println(OnlineShopping.getItemDetail(7));
			System.out.println(OnlineShopping.getItemDetail(8));
			System.out.println(OnlineShopping.getItemDetail(10));
			//connection close
		} catch (RuntimeException e) {
			System.out.println("Something's not right! - Divide by 0 excepiton" + e.getMessage());
		} catch (Exception e) {
			System.out.println("Something's not right! - " + e.getMessage());
		} finally {
			//connection close
			System.out.println("Wrapping job ...");
		}

	}

}
