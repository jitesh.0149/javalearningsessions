package sessions.day04.abstract_classes;

public abstract class Vehicle {

	int gasCapacity;
	int horsePower;
	boolean vehicleStarted;

	public abstract void transport();

	public abstract void start();

	public abstract void stop();

	public abstract void lock();

	public Vehicle(int gasCapacity, int horsePower) {
		super();
		this.gasCapacity = gasCapacity;
		this.horsePower = horsePower;
	}

	public void blowHorn() {
		if (vehicleStarted) {
			System.out.println("Blowing horn");
		} else {
			System.out.println("Please start the vehicle first");
		}
	}

}
