package sessions.day04.abstract_classes;

public class SwiftCar extends Vehicle {

	public SwiftCar(int gasCapacity, int horsePower) {
		super(gasCapacity, horsePower);
	}

	@Override
	public void transport() {
		System.out.println("Four wheeler transport");

	}

	@Override
	public void start() {
		System.out.println("Press start button");

	}

	@Override
	public void stop() {
		System.out.println("Press stop button");
	}

	@Override
	public void lock() {
		System.out.println("Locking using keys");
	}

	public void playMusic() {
		System.out.println("Playing Music");
	}

	public void turnOnAC() {
		System.out.println("Turning ON ac");
	}

	public void turnOnAC(int temp) {
		System.out.println("Turning ON ac with temprature " + temp);
	}

}
