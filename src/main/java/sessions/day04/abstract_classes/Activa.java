package sessions.day04.abstract_classes;

public class Activa extends Vehicle {

	public Activa(int gasCapacity, int horsePower) {
		super(gasCapacity, horsePower);
	}

	@Override
	public void transport() {
		System.out.println("Two wheeler transport");

	}

	@Override
	public void start() {
		vehicleStarted = true; 
		System.out.println("Need to kick (not a self-start vehicle");

	}

	@Override
	public void stop() {
		vehicleStarted = false;
		System.out.println("Using key");
	}

	@Override
	public void lock() {
		System.out.println("Locking using keys");
	}

	public void chargeMobile() {
		System.out.println("Charging mobile ...");
	}

}
