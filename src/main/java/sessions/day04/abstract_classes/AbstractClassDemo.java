package sessions.day04.abstract_classes;

public class AbstractClassDemo {

	public static void main(String[] args) {
		Activa a = new Activa(4, 125);
		SwiftCar s = new SwiftCar(10, 1000);
		a.start();
		a.blowHorn();
		a.stop();
		
		
		s.start();
		s.stop();
		s.turnOnAC();
		
		
	}

}
