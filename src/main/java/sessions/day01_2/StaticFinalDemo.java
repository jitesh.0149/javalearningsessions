package sessions.day01_2;

public class StaticFinalDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// ToCheckStaticAndFinal t = new ToCheckStaticAndFinal();
		// t.fname = "Test";
		ToCheckStaticAndFinal withArg = new ToCheckStaticAndFinal("Test Name");
		withArg = new ToCheckStaticAndFinal("Test Name1");
		withArg = new ToCheckStaticAndFinal("Test Name1");
		ToCheckStaticAndFinal withoutArg = new ToCheckStaticAndFinal();

		SubClass subClass = new SubClass();
		subClass.doWork();
		
		ToCheckStaticAndFinal.joinAParty();
		withArg.joinAParty();
		
	}

}

class ToCheckStaticAndFinal {
	final String fname;
	final static String VARIABLE_1;

	static {
		System.out.println("Program Started .....");
		VARIABLE_1 = "DEFAULT VALUE";
	}

	public ToCheckStaticAndFinal(String fname) {
		super();
		System.out.println("Instance Created .....");
		this.fname = fname;		
	}

	public ToCheckStaticAndFinal() {
		super();
		this.fname = "Test";
	}

	public final void doWork() {
		System.out.println("Always start working in morning ....");
	}
	
	public static void joinAParty() {
		System.out.println("Enjoying ....");
	}
	

}

class SubClass extends ToCheckStaticAndFinal {
	//@Override //Will throw exception as parent method is final
//	public void doWork() {
//		System.out.println("Always start working in evening ....");
//	}
}