package sessions.day01_2;

public class MainProgram {

	public static void main(String[] args) {
		// Starting point
		CanadianHuman rutvik = new CanadianHuman("9394-434", "Rutvik", "Shah");
		CanadianHuman saurabh = new CanadianHuman("3434-434", "Saurabh", "Rathod");

		CanadianHuman newHuman = new CanadianHuman();
		newHuman.fname = "test";

		IndianHuman kunal = new IndianHuman("LKJFLDSF", "Kunal", "Shah");

		System.out.println("abcd : " + rutvik.sinNo);
		System.out.println("First Name: " + rutvik.fname);

		System.out.println("First Name: " + newHuman.fname);
		System.out.println("Sin No: " + newHuman.sinNo);
		System.out.println("India Human: " + kunal.aadharCard);

		rutvik.earningRatio = 2;

		System.out.println(rutvik.getEarning());

	}

}
