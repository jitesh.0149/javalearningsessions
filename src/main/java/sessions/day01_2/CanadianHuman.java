package sessions.day01_2;

public class CanadianHuman extends Human {

	String sinNo;
	int earningRatio;
	int steps;
	final static String countryName = "CN";

	public int getEarning() {
		return earningRatio * 60000;
	}

	protected void takeAStep() {
		steps = steps + 1;
	}

	public void takeSteps(int n) {
		// validation for taking a step
		for (int i = 0; i < n; i++) {
			takeAStep(); // equals steps = steps + 1;
		}
	}

	public void takeSteps(int n, boolean forward) {
		// validation for taking a step
		for (int i = 0; i < n; i++) {
			if (forward) {
				steps = steps + 1;
			} else {
				steps = steps - 1;
			}
		}
	}

	public CanadianHuman() {
	}

	@Override
	public void eatFood() {
		System.out.println("I will eat meat too...");
	}

	public CanadianHuman(String sinNo, String fname, String lname) {
		super(fname, lname);
		this.sinNo = sinNo;
	}

	@Override
	public String toString() {
		return "CanadianHuman [sinNo=" + sinNo + ", earningRatio=" + earningRatio + ", steps=" + steps + "]";
	}
	
	

}
