package sessions.day01_2.pack1;

import sessions.day01_2.Human;

public class IndianHuman1 extends Human {

	String aadharCard;

	public IndianHuman1(String aadharCard, String fname, String lname) {
		super(fname, lname);
		this.aadharCard = aadharCard;
		System.out.println(this.fname);
	}
	
	public void test() {
		System.out.println(this.fname);
	}
}
