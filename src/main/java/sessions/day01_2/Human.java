package sessions.day01_2;

public class Human {
	public String fname;
	protected String lname;

	public Human() {

	}

	public Human(String fname, String lname) {
		this.fname = fname;
		this.lname = lname;
	}
	
	public void eatFood() {
		System.out.println("Don't kill animal, and eat vegetables");
	}
}
