package sessions.day01_2;

public class ReferenceVariableDemo {

	public static void main(String[] args) {
		CanadianHuman ch = new CanadianHuman("233", "Siddharth", "Sonera");
		CanadianHuman ch1 = new CanadianHuman("233", "Siddharth1", "Sonera1");
		CanadianHuman ch2 = ch;

		System.out.println(ch.fname);
		System.out.println(ch2.fname);

		ch2.fname = "Sidddd";
		System.out.println(ch.fname);
		System.out.println(ch2.fname);

//		ch2.takeAStep();
//		ch2.takeAStep();
		ch2.takeSteps(10);

		System.out.println(ch2.steps);
		ch2.takeSteps(5, false);
		System.out.println(ch2.steps);
		
		
		ch2.eatFood();
		IndianHuman i1 = new IndianHuman("22", "22", "33");
		i1.eatFood();
		
		System.out.println("Country Name :"+ch2.countryName);
		//ch2.countryName = "CH"; will give error as it is final
		System.out.println("Country Name :"+CanadianHuman.countryName);
		

	}

}
