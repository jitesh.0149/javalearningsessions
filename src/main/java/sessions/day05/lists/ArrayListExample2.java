package sessions.day05.lists;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ArrayListExample2 {
	public static void main(String[] args) {
		List<String> names = new ArrayList<String>();
		// select name from employees order by name
		names.add("Rutvik");
		names.add("Saurabh");
		names.add("Saurabh1");
		names.add("Jitesh");
		names.add("Yuvraj");
		// we can easily get value on an index
		System.out.println(names.get(2));

		System.out.println("Before Sorting " + names);
		Collections.sort(names);
		System.out.println("After Sorting " + names);

	}
}
