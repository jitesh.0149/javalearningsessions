package sessions.day05.lists;

import java.util.ArrayList;
import java.util.List;

import sessions.day01_2.CanadianHuman;

public class ArrayListExample3 {
	public static void main(String[] args) {
		List<CanadianHuman> humans = new ArrayList<CanadianHuman>();
		// makes an db call -- SELECT * FROM EMPLOYEE // DROP EMPLOYEE;
		CanadianHuman ch = new CanadianHuman("344", "Rutvik", "Shah");
		humans.add(ch);
		humans.add(new CanadianHuman("345", "Yuvraj", "Rathod"));
		humans.add(new CanadianHuman("346", "Jitesh", "Jadav"));

		//Collections.sort(humans);

	}
}
