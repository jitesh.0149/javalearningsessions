package sessions.day05.lists;

import java.util.ArrayList;
import java.util.List;

public class ArrayListExample {
	public static void main(String[] args) {
		List<String> names = new ArrayList<String>();

		names.add("Rutvik");
		names.add("Saurabh");

		System.out.println(names);

		names.remove("Saurabh");
		System.out.println(names);
		names.add("Saurabh");
		names.add("Yuvraj");

		System.out.println("Iterating over list ...");
		for (int i = 0; i < names.size(); i++) {
			System.out.println(names.get(i));
		}

		System.out.println("Iterating over list (other way)...");

		for (String a : names) {
			System.out.println(a);
			if (a.equals("Saurabh")) {
				System.out.println("Found element .....");
				break;
			}
		}

		// compare to array
		// - we don't need to specify the size
		// - we get lot more operations available (e.g. add, remove)
	}
}
