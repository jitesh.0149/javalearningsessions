package sessions.day05.lists;

public class ArrayExample {

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		String a = "Test";
		String[] arr = new String[10];
		arr[0] = "Zero";
		arr[1] = "One";
		

		System.out.println(arr[0]);
		System.out.println(arr[1]);

		String[][] arr1 = new String[10][20];
		arr1[0][0] = "Zero";
		arr1[0][1] = "One";

		System.out.println(arr1[0][0]);
		System.out.println(arr1[0][1]);

		String fname = "Saurabh";

		String[] fnames = new String[10];
		String[] tempFnames = new String[20];
		// fnames = //fetch names from database SELECT FNAME FROM EMPLOYEE
		fnames[0] = "Fname1";
		fnames[1] = "Fname2";
		fnames[2] = "Fname3";
		System.out.println("Iterate using for loop ....");
		for (int i = 0; i < fnames.length; i++) {
			System.out.println(fnames[i]);
			tempFnames[i] = fnames[i];
		}

	}

}
