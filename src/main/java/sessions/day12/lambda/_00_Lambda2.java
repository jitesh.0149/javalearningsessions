package sessions.day12.lambda;

public class _00_Lambda2 {
	public static void main(String[] args) {
		Test2 test = (manish) -> System.out.println(manish);
		test.testMethod("Hi");

		Test3 test3 = (a, b, message) -> System.out.println(message + ":" + (a + b));
		test3.testMethod(2, 4, "Output is");

		Test4 test4 = (a, b) -> a * b * 2;
		System.out.println(test4.multiplyTest(3, 5));

		Test4 test5 = (a, b) -> {
			int c = a * b * 2;
			System.out.println("New " + c);
			return c;
		};
		test5.multiplyTest(3, 5);
	}
}

@FunctionalInterface
interface Test2 {
	public void testMethod(String param);
}

interface Test3 {
	public void testMethod(Integer p1, Integer p2, String p3);
}

interface Test4 {
	public Integer multiplyTest(Integer p1, Integer p2);
}
