package sessions.day12.lambda;

public class _00_Lambda1 {
	public static void main(String[] args) {

		Test t = new Test() {
			@Override
			public void testMethod() {
				System.out.println("Hello");

			}
		};
		Test ti = new TestImpl();

		executeTestMethod(ti);
		executeTestMethod(t);


		executeTestMethod(new Test() {
			@Override
			public void testMethod() {
				System.out.println("Before Lambda Hello");

			}
		});

		executeTestMethod(() -> System.out.println("New Hello !!"));

	}

	public static void executeTestMethod(Test t) {
		t.testMethod();
	}
}

@FunctionalInterface
interface Test {
	public void testMethod();
}

//1st Way
class TestImpl implements Test {

	@Override
	public void testMethod() {
		System.out.println("HI");
	}

}