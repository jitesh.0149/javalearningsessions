package sessions.day06.hashmap;

public class EqualsEample {

	public static void main(String[] args) {
		Employee emp1 = new Employee(1,"Rutvik","Shah");
		Employee emp2 = new Employee(1,"Kunal","Shah");
		Employee emp3 = emp1;
		
		
		boolean areEquals = emp1.equals(emp2);
		System.out.println(areEquals);
		System.out.println(emp1.equals(emp3));
	}

}
