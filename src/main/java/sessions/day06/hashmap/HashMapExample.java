package sessions.day06.hashmap;

import java.util.HashMap;
import java.util.Map;

public class HashMapExample {

	public static void main(String[] args) {
		Map<Integer, String> map1 = new HashMap<Integer, String>();
		map1.put(20, "Rutvik");
		map1.put(21, "Yuvraj");
		map1.put(30, "Manish");
		map1.put(20, "Saurabh");

		System.out.println(map1.get(10));
		System.out.println(map1.get(21));
		System.out.println(map1.get(20));
	}

}
