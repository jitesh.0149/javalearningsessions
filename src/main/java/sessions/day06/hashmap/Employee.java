package sessions.day06.hashmap;

public class Employee {
	public Integer empNo;
	public String fname;
	public String lname;

	public Employee(Integer empNo, String fname, String lname) {
		super();
		this.empNo = empNo;
		this.fname = fname;
		this.lname = lname;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((empNo == null) ? 0 : empNo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (empNo == null) {
			if (other.empNo != null)
				return false;
		} else if (!empNo.equals(other.empNo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Employee [empNo=" + empNo + ", fname=" + fname + ", lname=" + lname + "]";
	}

}
