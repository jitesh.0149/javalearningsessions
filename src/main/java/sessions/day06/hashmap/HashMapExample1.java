package sessions.day06.hashmap;

import java.util.HashMap;
import java.util.Map;

public class HashMapExample1 {

	public static void main(String[] args) {
		Map<Employee, Project> employeeProjectsMap = new HashMap<Employee, Project>();

		Employee emp1 = new Employee(1, "Rutvik", "Shah");
		Project proj1 = new Project("1", "BI");

		employeeProjectsMap.put(emp1, proj1);
	}

}
