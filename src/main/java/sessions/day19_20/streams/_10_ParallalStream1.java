package sessions.day19_20.streams;

import java.util.stream.IntStream;
//When to use parallel streams - https://stackoverflow.com/questions/20375176/should-i-always-use-a-parallel-stream-when-possible
//https://blog.krecan.net/2014/03/18/how-to-specify-thread-pool-for-java-8-parallel-streams/
//https://stackoverflow.com/questions/52287717/how-to-specify-forkjoinpool-for-java-8-parallel-stream

//Below example explains that parallel stream uses the common ForkJoinPool and which would be equal to no. of processor on a machine
// Also it uses the common commonPool from the ForkJoinPool https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ForkJoinPool.html#commonPool--
public class _10_ParallalStream1 {
	public static void main(String[] args) {
		IntStream.range(1, 16).parallel().forEach(
				(value) -> {
					System.out.println(Thread.currentThread().getName() + " value: " + value);
					try {
						Thread.sleep(5000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					

				});
	}
}
