package sessions.day19_20.streams;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

//For more reference : https://www.baeldung.com/java-collection-stream-foreach
public class _01_ForEachExample {
	public static void main(String[] args) {
		Map<String, Integer> items = new HashMap<>();
		items.put("A", 10);
		items.put("B", 20);
		items.put("C", 30);
		items.put("D", 40);
		items.put("E", 50);
		items.put("F", 60);
		// First Way
		items.forEach((k, v) -> System.out.println("Item : " + k + " Count : " + v));
		// Second Way
		items.forEach((k, v) -> {
			System.out.println("Item : " + k + " Count : " + v);
			if ("E".equals(k)) {
				System.out.println("Hello E");
			}
		});

		// Third way
		BiConsumer<String, Integer> biConsumer = (k, v) -> {
			System.out.println("Third way Consumer Item : " + k + " Count : " + v);
		};

		items.forEach(biConsumer);

		List<String> list = Arrays.asList(new String[] { "A", "E", "I", "O", "U" });
		Consumer<String> consumer = (val) -> {
			System.out.println("String value is : " + val);
		};
		list.stream().forEach(consumer);
		System.out.println("forEach is available without stream,"
				+ " though both has different implementation");
		list.forEach(consumer);

	}
}
