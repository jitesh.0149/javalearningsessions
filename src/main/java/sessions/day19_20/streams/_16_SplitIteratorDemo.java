package sessions.day19_20.streams;

import java.util.ArrayList;
import java.util.Spliterator;
import java.util.stream.Stream;

/*Although a stream is not a data storage object, you can still use an iterator to cycle through
its elements in much the same way as you would use an iterator to cycle through the elements
of a collection. The stream API supports two types of iterators. The first is the traditional
Iterator. The second is Spliterator, which was added by JDK 8. It provides significant
advantages in certain situations when used with parallel streams.
*/

// Although SplitIterator is not part of Stream API but kept here for reference
// The Spliterator API supports primitive values including double, int and long.
// Must Read: https://howtodoinjava.com/java/collections/java-spliterator/
// https://www.baeldung.com/java-spliterator
class _16_SplitIteratorDemo {
	@SuppressWarnings("static-access")
	public static void main(String[] args) {

		// Create a list of Strings.
		ArrayList<String> myList = new ArrayList<>();
		myList.add("Alpha");
		myList.add("Beta");
		myList.add("Gamma");
		myList.add("Delta");
		myList.add("Phi");
		myList.add("Omega");
		//From source code public static final int SIZED      = 0x00000040; //Hexadecimal which is 64 in decimal
		System.out.println(myList.spliterator().SIZED);
		System.out.println(myList.spliterator().SUBSIZED);
		
		
		// Obtain a Stream to the array list.
		Stream<String> myStream = myList.stream();
		// Obtain a Spliterator.
		Spliterator<String> splitItr = myStream.spliterator();
		Spliterator<String> splitItr1 = splitItr.trySplit();
		//splitItr.trySplit();
		// Iterate the elements of the stream.
		while (splitItr.tryAdvance((n) -> System.out.println(n)))
			;
		
		while (splitItr1.tryAdvance((n) -> System.out.println(n)))
			;

	}
}