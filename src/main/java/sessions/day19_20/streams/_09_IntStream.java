package sessions.day19_20.streams;

import java.util.stream.IntStream;

//https://www.youtube.com/watch?v=N3gQdIn90CI&list=PLhfHPmPYPPRmwlUqOv4eDhMOF6IUXCWDJ&index=2&t=0s
public class _09_IntStream {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		IntStream.of(20).min().ifPresent(System.out::println);
		IntStream.of(20).min().ifPresent(p -> System.out.println(p));
		IntStream.of(20).max().ifPresent(p -> System.out.println(p));
		IntStream.of(20).average().ifPresent(p -> System.out.println(p));
		System.out.println(IntStream.range(0, 10).sum());
		System.out.println(IntStream.range(0, 10).summaryStatistics());
		System.out.println(IntStream.range(0, 10).skip(3).count());
		int[] temp = IntStream.range(0, 10).toArray();
		System.out.println(temp[2]);
		// Print first 3 sorted element
		int[] numbers = { 2, 3, 8, 0, 2, 0, 1 };
		IntStream.of(numbers)
				.distinct()
				.sorted()
				.limit(3)
				.forEach(System.out::println);
	}

}
