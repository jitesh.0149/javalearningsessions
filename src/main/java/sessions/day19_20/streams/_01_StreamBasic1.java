package sessions.day19_20.streams;

import java.util.List;
import java.util.stream.Stream;

import sessions.day16.lambda.functioninterfaces.Person;

public class _01_StreamBasic1 {

	static public List<Person> roster;

	public static void main(String[] args) {
		roster = Person.getDefautList();
		roster
				.stream()
				.filter(
						p -> p.getGender() == Person.Sex.MALE
								&& p.getAge() >= 18
								&& p.getAge() <= 25)
				.map(p -> p.getEmailAddress())
				.forEach(email -> System.out.println(email));

		Stream<Person> p = roster.stream();
		p.map(person -> person).forEach(person -> System.out.print(person.getEmailAddress()));

		// Below two statements which is division of above statement give
		// java.lang.IllegalStateException: stream has already been operated upon or closed
		// So keep in mind that you can consume a stream only once!

		// p.map(person -> person);
		// p.forEach(person -> System.out.print(person.getEmailAddress()));
	}

}