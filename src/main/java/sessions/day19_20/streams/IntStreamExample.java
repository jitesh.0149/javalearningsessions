package sessions.day19_20.streams;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.IntPredicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

//reference: https://stackoverflow.com/questions/18552005/is-there-a-concise-way-to-iterate-over-a-stream-with-indices-in-java-8
public class IntStreamExample {
	public static void main(String[] args) {



		String[] names = { "Sam", "Pamela", "Dave", "Pascal", "Erik" };
		IntStream.range(0, names.length)
				.filter(i -> names[i].length() <= i)
				.mapToObj(i -> names[i])
				.collect(Collectors.toList());

		// Another example
		AtomicInteger index = new AtomicInteger();
		@SuppressWarnings("unused")
		List<String> list = Arrays.stream(names)
				.filter(n -> n.length() <= index.incrementAndGet())
				.collect(Collectors
						.toList());
		
		// https://howtodoinjava.com/java8/intstream-examples/
		IntStream stream = IntStream.range(1, 100);

		List<Integer> primes = stream.filter(IntStreamExample::isPrime)
				.boxed()
				.collect(Collectors.toList());

		System.out.println(primes);
	}

	public static boolean isPrime(int i) {
		IntPredicate isDivisible = index -> i % index == 0;
		return i > 1 && IntStream.range(2, i).noneMatch(isDivisible);
	}
}
