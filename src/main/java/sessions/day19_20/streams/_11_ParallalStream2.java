package sessions.day19_20.streams;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

public class _11_ParallalStream2 {
	public static void main(String[] args) {
		ExecutorService es = Executors.newCachedThreadPool();

		es.submit(() -> {
			IntStream.range(1, 16).parallel().forEach(
					(value) -> {
						System.out.println(Thread.currentThread().getName() + " value: " + value);
						try {
							Thread.sleep(60000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					});
		});
		

		es.submit(() -> {
			IntStream.range(1, 16).parallel().forEach(
					(value) -> {
						System.out.println(Thread.currentThread().getName() + " value12: " + value);
						try {
							Thread.sleep(60000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					});
		});
	}
}