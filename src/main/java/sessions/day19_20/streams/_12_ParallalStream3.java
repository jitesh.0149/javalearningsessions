package sessions.day19_20.streams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
//https://www.youtube.com/watch?v=CEVDijyYiug
//https://dzone.com/articles/think-twice-using-java-8
//https://stackoverflow.com/questions/7926864/how-is-the-fork-join-framework-better-than-a-thread-pool/37257685#37257685
//https://stackoverflow.com/questions/37512662/is-there-anything-wrong-with-using-i-o-managedblocker-in-java8-parallelstream
//https://docs.oracle.com/javase/tutorial/collections/streams/parallelism.html
//https://www.netjstech.com/2017/01/reduction-operations-in-java-stream-api.html
import java.util.Optional;

//https://stackoverflow.com/questions/21163108/custom-thread-pool-in-java-8-parallel-stream
class _12_ParallalStream3 {
	public static void main(String[] args) {
		// This is now a list of double values.
		List<Double> myList = new ArrayList<>();
		myList.add(16.0);
		myList.add(4.0);
		myList.add(9.0);
		myList.add(16.0);
		myList.add(36.0);
		myList.add(1.0);

		// Identity � an element that is the initial value of the reduction operation and the default result if the stream is empty
		// The identity value must be an identity for the accumulator function.
		// This means that for all t, accumulator.apply(identity, t) is equal to t.
		// The accumulator function must be an associative function.
		
		// 4*2*3*4*6*1 = 576
		double productOfSqrRoots = myList.parallelStream().reduce(
				1.0,
				(a, b) -> sqrtTemp(a, b),
				(a, b) -> a * b);
		System.out.println("Product of square roots: " + productOfSqrRoots);
		// This won't work. (without the combilner (third parameter))
		// This will give result as wrong result. 
		// Why that happened is because not specifying a combiner means 
		// accumulator function itself will be used as combiner function too. In that case
		// when partial results are combined the square root is done again resulting in wrong value.
		double productOfSqrRoots2 = myList.parallelStream().reduce(
				1.0,
				(a, b) -> sqrtTemp(a, b));
		double productOfSqrRoots22 = myList.stream().reduce(
				1.0,
				(a, b) -> sqrtTemp(a, b));

		System.out.println("Product of square roots (Wrong output) : " + productOfSqrRoots2);
		System.out.println("Product of square roots (With Stream) : " + productOfSqrRoots22);

		myList = Arrays.asList(2.0, 6.0, 3.0);
		Optional<Double> productOfSqrRoots3 = myList.parallelStream().reduce(
				(a, b) -> a * b);

		System.out.println("Simple product : " + productOfSqrRoots3);
		// To Concvert parallel to sequential
		// myList.stream().parallel().sequential()

		/*
		 * 
		 * There is one other aspect of a stream to keep in mind when using parallel execution:
		 * the order of the elements. Streams can be either ordered or unordered. In general, if the
		 * data source is ordered, then the stream will also be ordered. However, when using a parallel
		 * stream, a performance boost can sometimes be obtained by allowing a stream to be
		 * unordered. When a parallel stream is unordered, each partition of the stream can be operated
		 * on independently, without having to coordinate with the others. In cases in which the order
		 * of the operations does not matter, it is possible to specify unordered behavior by calling the
		 * unordered( ) method, shown here:
		 * S unordered( )
		 * One other point: the forEach( ) method may not preserve the ordering of a parallel stream.
		 * If you want to perform an operation on each element in a parallel stream while preserving
		 * the order, consider using forEachOrdered( ). It is used just like forEach( ).
		 */
	}
	
	
	//Made new method to debug
	public static Double sqrtTemp(Double a, Double b) {
		return a * Math.sqrt(b);
	}
}