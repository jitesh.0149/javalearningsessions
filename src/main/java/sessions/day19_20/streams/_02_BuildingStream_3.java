package sessions.day19_20.streams;

import java.util.function.UnaryOperator;
import java.util.stream.Stream;

public class _02_BuildingStream_3 {
	public static void main(String[] args) {
		// Streams from functions: creating infinite streams!
		Stream.iterate(0, n -> n + 2)
				.limit(10)
				.forEach(System.out::println);
		// Detailed Implementation
		Stream.iterate(0, new UnaryOperator<Integer>() {

			@Override
			public Integer apply(Integer t) {
				return t + 2;
			}
		})
				.limit(10)
				.forEach(System.out::println);

		// Fibonacci series
		Stream.iterate(new int[] { 0, 1 }, t -> new int[] { t[1], t[0] + t[1] })
				.limit(20)
				.forEach(t -> System.out.println("(" + t[0] + "," + t[1] + ")"));

	}
}
