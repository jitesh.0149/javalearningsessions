package sessions.day19_20.streams;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import sessions.day16.lambda.functioninterfaces.Person;

public class _06_Collectors1 {

	public static void main(String[] args) {
		List<Person> persons = Person.getDefautList();
		System.out.println(converListToMap(persons));
		System.out.println(converListToList(persons));
	}

	public static Map<String, Person> converListToMap(List<Person> list) {

		// Below toMap returns HashMap by default and uses throwingMerger() as merger function (i.e. will throw duplicate key exception on duplicate key occurance)
		// A merger function is useful to resolve the duplicate key and return the desired object
		Map<String, Person> map = list.stream()
				.collect(Collectors.toMap(Person::getEmailAddress, p -> p));
		// //Alternative
		// Map<Integer, Person> map = list.stream()
		// .collect(Collectors.toMap(Person -> Person.id, Person -> Person));
		return map;
	}

	public static List<Person> converListToList(List<Person> list) {
		// returns arraylist
		return list.stream().map(t->t)
				.collect(Collectors.toList());
	}
}
