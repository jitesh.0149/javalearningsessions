package sessions.day19_20.streams;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.IntStream;


//Using custom ForkJoinPool
public class _11_ParallalStream3 {
	public static void main(String[] args) throws InterruptedException, ExecutionException {

		long begining = System.nanoTime();
		// ForkJoinPool es = new ForkJoinPool(Runtime.getRuntime().availableProcessors());
		ForkJoinPool es = new ForkJoinPool(16);
		ExecutorService es1 = Executors.newCachedThreadPool();
		es1.submit(() -> {
			try {
				es.submit(() -> {
					IntStream.range(1, 16).parallel().forEach(
							(value) -> {

								try {
									Thread.sleep(6000);
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								System.out.println(Thread.currentThread().getName() + " value: " + value + " time taken " + ((System.nanoTime() - begining) / 1000000));
							});

				}).get();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});

		es1.submit(() -> {
			try {
				es.submit(() -> {
					IntStream.range(1, 16).parallel().forEach(
							(value) -> {

								try {
									Thread.sleep(6000);
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								System.out.println(Thread.currentThread().getName() + " value: " + value + " time taken " + ((System.nanoTime() - begining) / 1000000));

							});
				}).get();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
	}
}