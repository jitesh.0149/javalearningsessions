package sessions.day19_20.streams;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;
import java.util.stream.Stream;

//https://stackoverflow.com/questions/22658322/java-8-performance-of-streams-vs-collections
public class _00_StreamPerformance {
	public static void main(String[] args) throws InterruptedException, ExecutionException {
		// Calculating square root of even numbers from 1 to N
		int min = 1;
		int max = 10000000;

		List<Integer> sourceList = new ArrayList<>();
		for (int i = min; i < max; i++) {
			sourceList.add(i);
		}

		List<Double> result = new LinkedList<>();

		// Collections approach
		long t0 = System.nanoTime();
		long elapsed = 0;
		for (Integer i : sourceList) {
			if (i % 2 == 0) {
				result.add(Math.sqrt(i));
			}
		}
		elapsed = System.nanoTime() - t0;
		System.out.printf("Collections: Elapsed time:\t\t\t\t %d ns \t(%f seconds)%n", elapsed, elapsed / Math.pow(10, 9));

		// Stream approach
		Stream<Integer> stream = sourceList.stream();
		t0 = System.nanoTime();
		result = stream.filter(i -> i % 2 == 0).map(i -> Math.sqrt(i)).collect(Collectors.toList());
		elapsed = System.nanoTime() - t0;
		System.out.printf("Streams: Elapsed time:\t\t\t\t\t %d ns \t(%f seconds)%n", elapsed, elapsed / Math.pow(10, 9));

		// Parallel stream approach
		stream = sourceList.stream().parallel();
		t0 = System.nanoTime();
		result = stream.filter(i -> i % 2 == 0).map(i -> Math.sqrt(i)).collect(Collectors.toList());
		elapsed = System.nanoTime() - t0;
		System.out.printf("Parallel streams: Elapsed time:\t\t\t\t %d ns \t(%f seconds)%n", elapsed, elapsed / Math.pow(10, 9));

		// Parallel stream approach - Custom ForkJoinPool
		ForkJoinPool fp = new ForkJoinPool(Runtime.getRuntime().availableProcessors());
		final Stream<Integer> stream1 = sourceList.stream().parallel();
		t0 = System.nanoTime();

		result = fp.submit(
				() -> stream1
						.filter(i -> i % 2 == 0)
						.map(i -> Math.sqrt(i)).collect(Collectors.toList()))
				.get();
		elapsed = System.nanoTime() - t0;
		System.out.printf("Parallel streams (Custom ForkJoinPool): Elapsed time:\t %d ns \t(%f seconds)%n", elapsed, elapsed / Math.pow(10, 9));

	}
}

/*
 * Output:  Stream much time were spent for stream initialization/values transmitting.
 *  
 * For max = 10
 * Collections: Elapsed time:      309000     ns (0.000309 seconds)
 * Streams: Elapsed time:          59933799   ns (0.059934 seconds)
 * Parallel streams: Elapsed time: 4326200    ns (0.004326 seconds)
 * 
 * For max = 100
 * Collections: Elapsed time:	   239000     ns (0.000239 seconds)
 * Streams: Elapsed time:		   44874500   ns (0.044875 seconds)
 * Parallel streams: Elapsed time: 3450700    ns (0.003451 seconds)
 * 
 * For max = 1000000
 * Collections: Elapsed time:      53728800   ns (0.053729 seconds)
 * Streams: Elapsed time:          425465600  ns (0.425466 seconds)
 * Parallel streams: Elapsed time: 32316000   ns (0.032316 seconds)
 * 
 * Collections: Elapsed time:				 				 921199500 ns 	(0.921200 seconds)
 * Streams: Elapsed time:					 				 279017200 ns 	(0.279017 seconds)
 * Parallel streams: Elapsed time:							 2260776500 ns 	(2.260777 seconds)
 * Parallel streams (Custom ForkJoinPool): Elapsed time:	 277214400 ns 	(0.277214 seconds)
 * 
 */
