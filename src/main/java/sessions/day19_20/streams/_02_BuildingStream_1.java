package sessions.day19_20.streams;

import java.util.stream.IntStream;
import java.util.stream.Stream;

public class _02_BuildingStream_1 {
	public static void main(String[] args) {
		// Using generate
		Stream.generate(Math::random)
				.limit(5)
				.forEach(System.out::println);
		
		IntStream ones = IntStream.generate(() -> 1);
		ones.limit(5).forEach(System.out::print);
	}
}
