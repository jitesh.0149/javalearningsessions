package sessions.day19_20.streams;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import sessions.day16.lambda.functioninterfaces.Person;

public class _05_StreamBasic3_FlatMap {

	static public List<Person> roster;

	public static void main(String[] args) {
		Stream.of('1', '2', '3')
				.map(ch -> Integer.parseInt(ch.toString()))
				.forEach(i -> System.out.print(i + ", "));
		System.out.println();

		// Use flatMap to convert from List<List<String>> to List<String
		// Program to demonstrate flatMap() method in Java 8 and above
		List<Integer> a = Arrays.asList(1, 2, 3);
		List<Integer> b = Arrays.asList(4, 5);
		List<Integer> c = Arrays.asList(6, 7, 8);

		List<List<Integer>> listOfListofInts = Arrays.asList(a, b, c);

		System.out.println("Before flattening : " + listOfListofInts);

		List<Integer> listofInts = listOfListofInts.stream()
				.flatMap(list -> list.stream())
				.collect(Collectors.toList());

		System.out.println("After flattening  : " + listofInts);

	}

}