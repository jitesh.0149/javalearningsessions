package sessions.day19_20.streams;

import java.util.List;

import sessions.day16.lambda.functioninterfaces.Person;

public class _04_StreamBasic2 {

	static public List<Person> roster;

	public static void main(String[] args) {
		roster = Person.getDefautList();

		// Returns whether this stream, if a terminal operation were to be executed,would execute in parallel. Calling this method after invoking anterminal stream operation method may yield unpredictable
		// results.
		System.out.println(roster
				.stream()
				.isParallel());

		// Some self-explained utilities
		System.out.println("findAny: " + roster.stream().findAny());
		System.out.println("allMatch: " + roster.stream().allMatch((p) -> p.getEmailAddress().contains("com")));
		System.out.println("noneMatch: " + roster.stream().noneMatch((p) -> p.getEmailAddress().contains("aa")));
		System.out.println("count: " + roster.stream().count());
		System.out.println("limit: " + roster.stream().limit(2).count());
		System.out.println("findFirst: " + roster.stream().findFirst());
		System.out.println("distinct: " + roster.stream().distinct().count());
		System.out.println("sorted: " + roster.stream().sorted((p1, p2) -> p1.getAge() - p2.getAge()).findFirst());
		System.out.println("max: " + roster.stream().max((p1, p2) -> p1.getAge() - p2.getAge()));
		System.out.println("min: " + roster.stream().min((p1, p2) -> p1.getAge() - p2.getAge()));
		// Closes this stream, causing all close handlers for this stream pipelineto be called.
		roster.stream().onClose(() -> System.out.println("Stream is closed")).close();
	}

}