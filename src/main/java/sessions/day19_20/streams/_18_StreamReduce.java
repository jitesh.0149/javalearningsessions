package sessions.day19_20.streams;

import java.util.List;
import java.util.function.BinaryOperator;

import sessions.day16.lambda.functioninterfaces.Person;

//https://www.baeldung.com/java-stream-reduce
/*
 * Before we look deeper into using the Stream.reduce() operation, let's break down the operation's participant elements into separate blocks. That way, we'll understand more easily the role that each one plays:

 * Identity � an element that is the initial value of the reduction operation and the default result if the stream is empty
 * Accumulator � a function that takes two parameters: a partial result of the reduction operation and the next element of the stream
 * Combiner � a function used to combine the partial result of the reduction operation when the reduction is parallelized, or when there's a mismatch between the types of the accumulator arguments and the types of the accumulator implementation
 */
public class _18_StreamReduce {

	static public List<Person> roster;

	public static void main(String[] args) {
		roster = Person.getDefautList();
		System.out.println(roster.stream().reduce(new BinaryOperator<Person>() {

			@Override
			public Person apply(Person t, Person u) {
				return (t.getAge() > u.getAge()) ? t : u;
			}
		}));
	}

}