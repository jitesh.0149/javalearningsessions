package sessions.day19_20.streams;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
//-Djava.util.concurrent.ForkJoinPool.common.parallelism=16
//https://stackoverflow.com/questions/21163108/custom-thread-pool-in-java-8-parallel-stream
//https://stackoverflow.com/questions/30802463/how-many-threads-are-spawned-in-parallelstream-in-java-8

/*A ForkJoinPool may be constructed with a given target parallelism level; by default, equal to the number of available processors.*/
public class _15_ParallalStream6 {
	public static void main(String[] args) {
		final int parallelism = 4;
		ForkJoinPool forkJoinPool = null;
		try {
			forkJoinPool = new ForkJoinPool(parallelism);
			final List<Integer> primes = forkJoinPool.submit(() ->

			// Parallel task here, for example
			IntStream.range(1, 1_000_000).parallel()
					// .filter(PrimesPrint::isPrime)
					.boxed().collect(Collectors.toList())).get();
			System.out.println(primes);
		} catch (InterruptedException | ExecutionException e) {
			throw new RuntimeException(e);
		} finally {
			if (forkJoinPool != null) {
				forkJoinPool.shutdown();
			}
		}
	}
}
