package sessions.day19_20.streams;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OperationsPerInvocation;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

@OutputTimeUnit(TimeUnit.NANOSECONDS)
@BenchmarkMode(Mode.AverageTime)
@OperationsPerInvocation(_00_StreamPerformance1.N)
//@Fork(value = 2, jvmArgs = {"-Xms2G", "-Xmx2G"})
//https://mkyong.com/java/java-jmh-benchmark-tutorial/
//https://stackoverflow.com/questions/22658322/java-8-performance-of-streams-vs-collections
public class _00_StreamPerformance1 {
	public static final int N = 1000000;

	public static void main(String[] args) throws RunnerException {

		Options opt = new OptionsBuilder()
				.include(_00_StreamPerformance1.class.getSimpleName())
				.forks(1)
				.build();

		new Runner(opt).run();
	}

	static List<Integer> sourceList = new ArrayList<>();
	static {
		for (int i = 0; i < N; i++) {
			sourceList.add(i);
		}
	}

//	@Benchmark
	public List<Double> vanilla() {
		List<Double> result = new ArrayList<>(sourceList.size() / 2 + 1);
		for (Integer i : sourceList) {
			if (i % 2 == 0) {
				result.add(Math.sqrt(i));
			}
		}
		return result;
	}

	@Benchmark
	public List<Double> stream() {
		return sourceList.stream()
				.filter(i -> i % 2 == 0)
				.map(Math::sqrt)
				.collect(Collectors.toCollection(
						() -> new ArrayList<>(sourceList.size() / 2 + 1)));
	}

	@Benchmark
	public List<Double> streamParallel() {
		System.out.println(Thread.currentThread());
		return sourceList.parallelStream()
				.filter(i -> i % 2 == 0)
				.map(Math::sqrt)
				.collect(Collectors.toCollection(
						() -> new ArrayList<>(sourceList.size() / 2 + 1)));
	}
}

/*
 * 
 * # JMH version: 1.23
 * # VM version: JDK 1.8.0_241, Java HotSpot(TM) 64-Bit Server VM, 25.241-b07
 * # VM invoker: C:\Program Files\Java\jdk1.8.0_241\jre\bin\java.exe
 * # VM options: -Dfile.encoding=Cp1252
 * # Warmup: 5 iterations, 10 s each
 * # Measurement: 5 iterations, 10 s each
 * # Timeout: 10 min per iteration
 * # Threads: 1 thread, will synchronize iterations
 * # Benchmark mode: Average time, time/op
 * 
 * 
 * Output for N=10
 * 
 * (Run - 1)
 * Benchmark                             Mode Cnt Score    Error      Units
 * _00_StreamPerformance1.stream         avgt 5   14.307   � 0.180    ns/op
 * _00_StreamPerformance1.streamParallel avgt 5   2630.075 � 889.102  ns/op
 * _00_StreamPerformance1.vanilla        avgt 5   14.010   � 10.072   ns/op
 * 
 * (Run - 2)
 * Benchmark                              Mode  Cnt     Score     Error  Units
 * _00_StreamPerformance1.stream          avgt    5    12.898 �   0.291  ns/op
 * _00_StreamPerformance1.streamParallel  avgt    5  2331.146 � 115.349  ns/op
 * _00_StreamPerformance1.vanilla         avgt    5     7.757 �   0.301  ns/op
 * 
 * Output for N = 100
 * Benchmark                              Mode  Cnt    Score    Error  Units
 * _00_StreamPerformance1.stream          avgt    5    6.539 �  0.254  ns/op
 * _00_StreamPerformance1.streamParallel  avgt    5  304.765 � 24.422  ns/op
 * _00_StreamPerformance1.streamParallel  avgt    5  261.893 � 33.062  ns/op  (With 8 Threads)
 * _00_StreamPerformance1.vanilla         avgt    5    5.700 �  0.287  ns/op
 * 
 * Output for N = 10000
 * Benchmark                              Mode Cnt Score    Error      Units
 * _00_StreamPerformance1.stream          avgt 5   7.747    � 0.538    ns/op
 * _00_StreamPerformance1.streamParallel  avgt 5   15.472   � 14.070   ns/op
 * _00_StreamPerformance1.vanilla         avgt 5   8.374    � 1.774    ns/op
 * 
 */
