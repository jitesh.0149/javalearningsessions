package sessions.day19_20.streams;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import sessions.day16.lambda.functioninterfaces.Person;
import sessions.day16.lambda.functioninterfaces.Person.Sex;
//https://www.baeldung.com/java-8-collectors
public class GroupingByExample {

	public static void main(String[] args) {
//1st Way
		Map<Person.Sex, List<Person>> byGender = Person.getDefautList()
				.stream()
				.collect(
						Collectors.groupingBy(Person::getGender));

		System.out.println(byGender);

		// 2nd Way

		byGender = Person.getDefautList()
				.stream()
				.collect(
						Collectors.groupingBy((Person p) -> {
							return p.getGender();
						}));

		System.out.println(byGender);
		// 3rd Way
		byGender = Person.getDefautList()
				.stream()
				.collect(
						Collectors.groupingBy(
								new Function<Person, Person.Sex>() {

									@Override
									public Sex apply(Person p) {
										// TODO Auto-generated method stub
										return p.getGender();
									}
								}));
		System.out.println(byGender);
	}

}
