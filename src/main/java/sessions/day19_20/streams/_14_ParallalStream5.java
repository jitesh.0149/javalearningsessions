package sessions.day19_20.streams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

//https://docs.oracle.com/javase/tutorial/collections/streams/parallelism.html
/*
 * 
 * Avoid using stateful lambda expressions as parameters in stream operations.
 * A stateful lambda expression is one whose result depends on any state that 
 * might change during the execution of a pipeline. The following example adds
 * elements from the List listOfIntegers to a new List instance with the map 
 * intermediate operation. 
 * It does this twice, first with a serial stream and then with a parallel stream:
 */

/*
 * The lambda expression e -> { parallelStorage.add(e); return e; } is a stateful lambda expression. 
 *  Its result can vary every time the code is run. 
 */
class _14_ParallalStream5 {
	public static void main(String[] args) {
		Integer[] intArray = { 1, 2, 3, 4, 5, 6, 7, 8 };
		List<Integer> listOfIntegers = new ArrayList<>(Arrays.asList(intArray));
		List<Integer> serialStorage = new ArrayList<>();

		System.out.println("Serial stream:");
		listOfIntegers
				.stream()

				// Don't do this! It uses a stateful lambda expression.
				.map(e -> {
					serialStorage.add(e);
					return e;
				})

				.forEachOrdered(e -> System.out.print(e + " "));
		System.out.println("");

		serialStorage
				.stream()
				.forEachOrdered(e -> System.out.print(e + " "));
		System.out.println("");

		System.out.println("Parallel stream:");
		List<Integer> parallelStorage = Collections.synchronizedList(
				new ArrayList<>());
		listOfIntegers
				.parallelStream()

				// Don't do this! It uses a stateful lambda expression.
				.map(e -> {
					parallelStorage.add(e);
					return e;
				})

				.forEachOrdered(e -> System.out.print(e + " "));
		System.out.println("");

		parallelStorage
				.stream()
				.forEachOrdered(e -> System.out.print(e + " "));
		System.out.println("");
	}
}