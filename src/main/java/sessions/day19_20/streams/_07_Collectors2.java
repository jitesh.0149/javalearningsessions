package sessions.day19_20.streams;

import java.util.List;
import java.util.stream.Collectors;

import sessions.day16.lambda.functioninterfaces.Person;

public class _07_Collectors2 {

	public static void main(String[] args) {
		// Below are some of the Collectors methods
		List<Person> persons = Person.getDefautList();
		System.out.println("Average age : " + persons.stream()
				.collect(Collectors.averagingInt((p) -> p.getAge())));

		// returns hashMap
		System.out.println("Grouping : " + persons.stream()
				.collect(Collectors.groupingBy((p) -> p.getAge())));
		System.out.println("Partition : " + persons.stream()
				.collect(Collectors.partitioningBy((p) -> p.getAge() > 26)));

		String listOfEmails = persons.stream().map(p -> p.getEmailAddress())
				.collect(Collectors.joining(", "));
		System.out.println(listOfEmails);

	}
}