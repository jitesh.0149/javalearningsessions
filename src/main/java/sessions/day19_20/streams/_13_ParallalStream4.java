package sessions.day19_20.streams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
//https://docs.oracle.com/javase/tutorial/collections/streams/parallelism.html
class _13_ParallalStream4 {
	public static void main(String[] args) {
		Integer[] intArray = { 1, 2, 3, 4, 5, 6, 7, 8 };
		List<Integer> listOfIntegers = new ArrayList<>(Arrays.asList(intArray));

		System.out.println("listOfIntegers:");
		listOfIntegers
				.stream()
				.forEach(e -> System.out.print(e + " "));
		System.out.println("");

		System.out.println("listOfIntegers sorted in reverse order:");
		Comparator<Integer> normal = Integer::compare;
		Comparator<Integer> reversed = normal.reversed();
		Collections.sort(listOfIntegers, reversed);
		listOfIntegers
				.stream()
				.forEach(e -> System.out.print(e + " "));
		System.out.println("");

		System.out.println("Parallel stream");
		listOfIntegers
				.parallelStream()
				.forEach(e -> System.out.print(e + " "));
		System.out.println("");

		System.out.println("Another parallel stream:");
		listOfIntegers
				.parallelStream()
				.forEach(e -> System.out.print(e + " "));
		System.out.println("");

		/*
		 * The fifth pipeline uses the method forEachOrdered, which processes the elements of 
		 * the stream in the order specified by its source, regardless of whether you executed
		 * the stream in serial or parallel.
		 * Note that you may lose the benefits of parallelism if you use operations like forEachOrdered with parallel streams.
		 */
		System.out.println("With forEachOrdered:");
		listOfIntegers
				.parallelStream()
				.forEachOrdered(e -> System.out.print(e + " "));
		System.out.println("");
	}
}