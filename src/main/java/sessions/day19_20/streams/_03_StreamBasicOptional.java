package sessions.day19_20.streams;

import java.util.List;
import java.util.Optional;

import sessions.day16.lambda.functioninterfaces.Person;

//Though Optional is part of java.util (introduced in JDK 8), just putting here to get understanding 
//https://www.tutorialspoint.com/java8/java8_optional_class.htm
public class _03_StreamBasicOptional {

	static public List<Person> roster;

	/*
	 * Optional is a container object used to contain not-null objects.
	 * Optional object is used to represent null with absent value. This class has various utility
	 * methods to facilitate code to handle values as �available� or �not available� instead of checking null values.
	 * It is introduced in Java 8 and is similar to what Optional is in Guava.
	 */
	public static void main(String[] args) {

		Optional<Integer> o1 = Optional.ofNullable(null);
		Optional<Integer> o2 = Optional.of(3);
		Optional<String> o3 = Optional.ofNullable(null);
		System.out.println("Optional values are o1: " + o1 + " o1.isPresent(): " + o1.isPresent() + " o2:" + o2 + " o3:" + o3);
		// Exception in thread "main" java.util.NoSuchElementException: No value present for below o1 and o3
		// System.out.println("Optional values are o1: " + o1.get() + " o2:" + o2.get() + " o3:" + o3.get());
		
		System.out.println("Optional values are o2.get(): " + o2.get());
		System.out.println(o2.isPresent());
		System.out.println("Sum of o1 & o2 " + sum(o1, o2));

	}

	public static Integer sum(Optional<Integer> a, Optional<Integer> b) {
		// Optional.isPresent - checks the value is present or not

		System.out.println("First parameter is present: " + a.isPresent());
		System.out.println("Second parameter is present: " + b.isPresent());

		// Optional.orElse - returns the value if present otherwise returns
		// the default value passed.
		Integer value1 = a.orElse(new Integer(0));

		// Optional.get - gets the value, value should be present
		Integer value2 = b.get();
		return value1 + value2;
	}
}
