package sessions.day19_20.streams;

import sessions.day16.lambda.functioninterfaces.Person;

public interface CheckPerson {
	boolean test(Person p);
}