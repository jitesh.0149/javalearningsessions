package sessions.day15_16.threads;

public class _05_ThreadGroup implements Runnable {
	public static void main(String[] args) {
		ThreadGroup g1 = new ThreadGroup("Group 1");
		Thread t1 = new Thread(g1, new _05_ThreadGroup());
		Thread t2 = new Thread(g1, new _05_ThreadGroup());
		Thread t3 = new Thread(g1, new _05_ThreadGroup());
		
		t1.start();
		t2.start();
		t3.start();
		
		g1 = new ThreadGroup("Group 2");
		t1 = new Thread(g1, new _05_ThreadGroup());
		t2 = new Thread(g1, new _05_ThreadGroup());
		t3 = new Thread(g1, new _05_ThreadGroup());
		t1.start();
		t2.start();
		t3.start();
	}

	@Override
	public void run() {	
		System.out.println(Thread.currentThread().getThreadGroup().getName() + " : " + Thread.currentThread().getName());
	}
	
}
