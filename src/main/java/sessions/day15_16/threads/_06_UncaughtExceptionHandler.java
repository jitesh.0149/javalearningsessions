package sessions.day15_16.threads;

public class _06_UncaughtExceptionHandler implements Runnable {
	public static void main(String[] args) throws InterruptedException {
		boolean[] exceptionOccurred = new boolean[10];
		
		Thread t1 = new Thread(new _06_UncaughtExceptionHandler(), "Test Thread");
		t1.setUncaughtExceptionHandler((t, e) -> {

			System.out.println("Thread Name: " + t.getName());
			System.out.println("Error Occurred");
			exceptionOccurred[0] = true;
		});
		t1.start();
		t1.join();
		System.out.println("I don't care with the errors : " + exceptionOccurred[0]);

	}

	@Override
	public void run() {
		System.out.println("I am started ...");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		@SuppressWarnings("unused")
		int i = 0, j = 1 / i;
	}
}
