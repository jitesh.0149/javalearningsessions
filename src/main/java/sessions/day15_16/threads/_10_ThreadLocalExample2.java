package sessions.day15_16.threads;

import java.util.function.Supplier;

public class _10_ThreadLocalExample2 implements Runnable {
	ThreadLocal<Integer> number1 = ThreadLocal.withInitial(new Supplier<Integer>() {
		@Override
		public Integer get() {
			return 0;
		}

	});
	//static variable
	static ThreadLocal<Integer> number2 = ThreadLocal.withInitial(new Supplier<Integer>() {
		@Override
		public Integer get() {
			return 0;
		}

	});
	Integer number3 = 0;

	public static void main(String[] args) {
		_10_ThreadLocalExample2 runnable = new _10_ThreadLocalExample2();
		for (int i = 0; i < 100; i++) {
			new Thread(runnable).start();
		}
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		number1.set(number1.get() + 1);
		System.out.println("Number1 (ThreadLocal): " + number1.get());
		number2.set(number2.get() + 1);
		System.out.println("Number2 (ThreadLocal - static): " + number2.get());
		number3 = number3 + 1;
		System.out.println("Number3 : " + number3);

	}
}
