package sessions.day15_16.threads;

public class _02_HelloRunnableLambda {

	public static void main(String args[]) {
		Thread t1 = new Thread(new Runnable() {

			@Override
			public void run() {
				System.out.println("Test");
				try {
					Thread.sleep(7000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				System.out.println("After Test");

			}
		});
		t1.start();

		Thread t2 = new Thread(() -> testMethod());
		t2.start();
		System.out.println("Test");
		System.out.println("test");
	}
	
	

	public static void testMethod() {

		System.out.println("Lambda Test");
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("LabmdaTest After");

	}
}
