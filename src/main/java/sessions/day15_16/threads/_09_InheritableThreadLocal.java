package sessions.day15_16.threads;

public class _09_InheritableThreadLocal {
	public static void main(String[] args) {
		ParentThread gfg_pt = new ParentThread();
		gfg_pt.start();
	}

}

//https://www.geeksforgeeks.org/java-lang-inheritablethreadlocal-class-examples/
class ParentThread extends Thread {
	// anonymous inner class for overriding childValue method.
	// used *** static *** here 
	public static InheritableThreadLocal<String> itl = new InheritableThreadLocal<String>();
	public static ThreadLocal<String> tl = new ThreadLocal<String>();
	// public static ThreadLocal<String> tl = ThreadLocal.withInitial(() -> "Default Value");

	public void run() {
		// setting the new value
		itl.set("parent data");
		tl.set("parent data");
		// parent data
		System.out.println("Parent Thread Value (itl) :" + itl.get() + " (tl) : " + tl.get());

		ChildThread gfg_ct = new ChildThread();
		gfg_ct.start();
	}
}

class ChildThread extends Thread {

	public void run() {
		// child data
		System.out.println("Child Thread Value (itl):" + ParentThread.itl.get() + " (tl) :" + ParentThread.tl.get());
	}
}
