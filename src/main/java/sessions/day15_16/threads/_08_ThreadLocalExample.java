package sessions.day15_16.threads;

public class _08_ThreadLocalExample implements Runnable {
	ThreadLocal<Integer> int1 = ThreadLocal.withInitial(() -> 0); // Initializing with 0
	int int2;

	@Override
	public synchronized void run() {
		//synchronized (this) {
			int2++; // int2 = int2 + 1;
			System.out.println(Thread.currentThread() + "default Int: " + int2);	
		//}
		

		int temp = int1.get();
		int1.set(++temp);

		System.out.println(Thread.currentThread() +"ThreadLocal :" + temp);

	}

	public static void main(String[] args) {
		_08_ThreadLocalExample threadLocalExample = new _08_ThreadLocalExample();

		int totalThreads = 20;
		for (int i = 0; i < totalThreads; i++) {
			Thread t1 = new Thread(threadLocalExample);
			t1.start();
		}

	}

}
