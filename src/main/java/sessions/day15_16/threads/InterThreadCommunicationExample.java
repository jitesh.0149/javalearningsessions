package sessions.day15_16.threads;

import java.util.LinkedList;
import java.util.Queue;
import java.util.logging.Logger;

//https://javarevisited.blogspot.com/2013/12/inter-thread-communication-in-java-wait-notify-example.html
public class InterThreadCommunicationExample {

	public static void main(String args[]) {

		final Queue<Integer> sharedQ = new LinkedList<>();

		Thread producer = new Producer(sharedQ);
		Thread consumer = new Consumer(sharedQ);

		producer.start();
		consumer.start();

	}
}

class Producer extends Thread {
	private static final Logger logger = Logger.getLogger(Producer.class.getName());
	private final Queue<Integer> sharedQ;

	public Producer(Queue<Integer> sharedQ) {
		super("Producer");
		this.sharedQ = sharedQ;
	}

	@Override
	public void run() {

		for (int i = 100; i < 400; i = i * 2) {
			synchronized (sharedQ) {
				// waiting condition - wait until Queue is not empty
				while (sharedQ.size() >= 1) {
					try {
						logger.info("Queue is full, waiting");
						sharedQ.wait();
					} catch (InterruptedException ex) {
						ex.printStackTrace();
					}
				}
				logger.info("producing : " + i);
				sharedQ.add(i);
				sharedQ.notify();
			}
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}

class Consumer extends Thread {
	private static final Logger logger = Logger.getLogger(Consumer.class.getName());
	private final Queue<Integer> sharedQ;

	public Consumer(Queue<Integer> sharedQ) {
		super("Consumer");
		this.sharedQ = sharedQ;
	}

	@Override
	public void run() {
		while (true) {

			synchronized (sharedQ) {
				// waiting condition - wait until Queue is not empty
				while (sharedQ.size() == 0) {
					try {
						logger.info("Queue is empty, waiting");
						sharedQ.wait();
					} catch (InterruptedException ex) {
						ex.printStackTrace();
					}
				}
				int number = sharedQ.poll();
				logger.info("consuming : " + number);
				sharedQ.notify();

				// termination condition
				if (number == 3) {
					break;
				}
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

			}
		}
	}
}
