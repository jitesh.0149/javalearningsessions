package sessions.day15_16.threads;

public class _04_waitNotify {
Integer i =0;
	public static void main(String[] args) throws InterruptedException {
		ThreadB b = new ThreadB();
		b.setName("Thread - B");
		b.start();
		
		synchronized (b) {
			try {
				System.out.println("Waiting for b to complete...");
				// b.interrupt(); // Then the exception will be thrown at line 34 - java.lang.InterruptedException: sleep interrupted
				b.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			System.out.println("Total is: " + b.total);
		}
	}
	
}

class ThreadB extends Thread {
	int total;

	@Override
	public void run() {
		synchronized (this) {
			for (int i = 0; i < 3; i++) {
				total++;
				// System.out.println(i);
				try {
					System.out.println(Thread.currentThread());
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			notify(); // if we comment this statement then also the the main thread will proceed once
						// threadB is completed
			
			
		}
		
		try {
			Thread.sleep(8000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("DB Updated");
	}
}
