package sessions.day15_16.threads;

import java.util.logging.Level;
import java.util.logging.Logger;

public class _07_VolatileTest {
	private static final Logger LOGGER = Logger.getLogger(_07_VolatileTest.class.getName());

	private static int MY_INT = 0;

	public static void main(String[] args) {
		new ChangeListener().start();
		new ChangeMaker().start();
	}

	static class ChangeListener extends Thread {
		@Override
		public void run() {
			int local_value = MY_INT;			
			System.out.println("Waiting");
			while (local_value < 5) {

				//System.out.println("Change Listener ....");
				if (local_value != MY_INT) {
					LOGGER.log(Level.INFO, "Got Change for MY_INT : {0}", MY_INT);
					local_value = MY_INT;
					
				}
				//System.out.println(local_value+" : "+MY_INT);
//				try {
//					Thread.sleep(500);
//				} catch (InterruptedException e) {
//					e.printStackTrace();
//				}
			}
		}
	}

	static class ChangeMaker extends Thread {
		@Override
		public void run() {

			//A write to a volatile field (�8.3.1.4) happens-before every subsequent read of that field.
			while (MY_INT < 5) {
				//System.out.println("Change Maker ....");
				int local_value = MY_INT;
				LOGGER.log(Level.INFO, "Incrementing MY_INT to {0}", local_value + 1);
				MY_INT = ++local_value;
				//MY_INT = ++MY_INT;
				//System.out.println("Incremented"+MY_INT);
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
			}
		}
	}
}