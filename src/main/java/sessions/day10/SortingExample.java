package sessions.day10;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SortingExample {

	public static void main(String[] args) {
		List<Integer> list = new ArrayList<>();
		list.add(550);
		list.add(1);
		list.add(10);
		list.add(20);
		
		CommonUtils.printList(list);
		
		
		Collections.sort(list);
		System.out.println("After sorting ...");
		CommonUtils.printList(list);

	}

}
