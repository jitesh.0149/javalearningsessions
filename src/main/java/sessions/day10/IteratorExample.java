package sessions.day10;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class IteratorExample {

	public static void main(String[] args) {
		List<Integer> list = new ArrayList<>();
		list.add(1);
		list.add(10);
		list.add(20);

		for (Integer element : list) {
			System.out.println(element);
			// list.remove(element); //java.util.ConcurrentModificationException
			// list.add(20); //java.util.ConcurrentModificationException
		}

		// Applicable to List, Set
		Iterator<Integer> it = list.iterator();
		while (it.hasNext()) {
			System.out.println(it.next());
			
			// it.remove(); //It's a valid statement
			// list.add(30); //java.util.ConcurrentModificationException
		}

		System.out.println("Printing List Iterator Data ...");
		ListIterator<Integer> lit = list.listIterator();
		while (lit.hasNext()) {
			//lit.hasPrevious()
			//lit.nextIndex()
			System.out.println(lit.next());
			// lit.remove(); //It's a valid statement
			// list.add(30); //java.util.ConcurrentModificationException
			lit.add(3333); 
			lit.add(5555); 
		}

		for (Integer element : list) {
			System.out.println(element);
		}

	}

}
