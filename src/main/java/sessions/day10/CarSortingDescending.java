package sessions.day10;

import java.util.Comparator;

public class CarSortingDescending implements Comparator<Car> {

	@Override
	public int compare(Car o1, Car o2) {

		return o2.horsePower - o1.horsePower;
	}

}
