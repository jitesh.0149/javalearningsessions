package sessions.day10;

public class Car implements Comparable<Car> {

	public int horsePower;
	public String brandName;

	public Car(int horsePower, String brandName) {
		super();
		this.horsePower = horsePower;
		this.brandName = brandName;
	}

	public Car() {
		super();
	}

	// output can be greater than, less than or equal to 0
	@Override
	public int compareTo(Car o) {
		return this.horsePower - o.horsePower;

		// return this.brandName.charAt(0) - o.brandName.charAt(0);
	}

	@Override
	public String toString() {
		return "Car [horsePower=" + horsePower + ", brandName=" + brandName + "]";
	}

}
