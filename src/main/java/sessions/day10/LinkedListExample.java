package sessions.day10;

import java.util.LinkedList;
import java.util.Queue;

public class LinkedListExample {

	public static void main(String[] args) {
		LinkedList<String> ll = new LinkedList<String>();
		ll.add("test");
		ll.add("test2");
		System.out.println(ll);
//		ll.pollLast();
//		ll.poll();

		System.out.println("First Element is: " + ll.get(1));
		System.out.println("First Element is : " + ll.peek());
		System.out.println(ll);
		//Visit - https://docs.oracle.com/javase/7/docs/api/java/util/Deque.html
		Queue<String> q = new LinkedList<>();
		q.add("test");
		q.poll();
	}

}
