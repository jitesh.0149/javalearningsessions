package sessions.day10;

import java.util.Comparator;

public class CarComparatorByName implements Comparator<Car> {

	@Override
	public int compare(Car o1, Car o2) {

		return o1.brandName.charAt(0) - o2.brandName.charAt(0);
	}

}
