package sessions.day10;

import java.util.PriorityQueue;

public class PriorityQueueExample {

	public static void main(String[] args) {
		PriorityQueue<Integer> q = new PriorityQueue<Integer>();
		q.add(55);
		q.add(20);
		q.add(40);
		q.add(10);
		q.add(15);
		System.out.println(q);
		System.out.println(q.poll());
		System.out.println(q.poll());

	}

}
