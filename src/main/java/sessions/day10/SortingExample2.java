package sessions.day10;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SortingExample2 {

	public static void main(String[] args) {
		List<Car> cars = new ArrayList<>();

		Car car = new Car();
		car.brandName = "Test";
		car.horsePower = 1200;
		cars.add(car);

		cars.add(new Car(5000, "Tesla"));
		cars.add(new Car(600, "Besla New"));
		cars.add(new Car(60088, "ATesla New"));

		CommonUtils.printList(cars);
		Collections.sort(cars, (c1, c2) -> c1.brandName.charAt(0) - c2.brandName.charAt(1));
		System.out.println("After sorting (name) ...");
		CommonUtils.printList(cars);
		System.out.println("After sorting (desc) ...");
		Collections.sort(cars, (c1, c2) -> c2.horsePower - c1.horsePower);
		CommonUtils.printList(cars);

	}

}
