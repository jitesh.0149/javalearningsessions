package sessions.day03.interfaces;

public class SonyTV implements NewTVRemote, NetFlixFeatures {

	@Override
	public void shutDown() {
		// Check if it is running any third party app (e.g. Netflix) and save the
		// content of last watched time
		// Shutdown the TV
		System.out.println("Shutting down the TV ....");
	}

	@Override
	public void restart() {
		System.out.println("Restart the TV ....");
	}

	@Override
	public void listOutEpisodes() {
		System.out.println("SONY TV - Searching online for the episodes");
	}
	
	
	public void test() {
		
	}

}
