package sessions.day03.interfaces;

public interface NetFlixFeatures {
	public static final String REMOTE_BATTERY = "ABCD";

	public void listOutEpisodes();

	public default void aDefaultProcedureSample() {
		System.out.println("Performing a default procedure..");
	}

	public default int doTheSum(int a, int b) {
		return a + b;
	}

}
