package sessions.day03.interfaces;

public interface NewTVRemote {

	public void shutDown();

	public void restart();

	//public void scheduleStart();

}
