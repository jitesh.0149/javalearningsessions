package sessions.day03.abstractclasses;

public class SonyTV extends UniversalTVRemote {

	@Override
	public void shutDown() {
		//Check if it is running any third party app (e.g. Netflix) and save the content of last watched time
		//Shutdown the TV
		
		System.out.println("Shutting down the TV ....");
	}

	@Override
	public void restart() {
		System.out.println("Restart the TV ....");
	}

}
