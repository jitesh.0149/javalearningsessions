package sessions.day03.abstractclasses;

abstract class UniversalTVRemote {

	String remoteName;

	abstract public void shutDown();

	abstract public void restart();

	public void newMethod() {
		System.out.println("I am an instance method :: " + remoteName);
	}

}
