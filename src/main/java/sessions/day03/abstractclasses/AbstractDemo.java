package sessions.day03.abstractclasses;

public class AbstractDemo {

	public static void main(String[] args) {
		SonyTV sonyTV = new SonyTV();
		SamsungTV samsungTV = new SamsungTV();
		sonyTV.shutDown();
		samsungTV.shutDown();

		UniversalTVRemote assembledTV = new UniversalTVRemote() {

			@Override
			public void shutDown() {
				System.out.println("My Own TV is Shutting Down");
			}

			@Override
			public void restart() {

			}
		};

		assembledTV.shutDown();
		SonyTV sonyTV1 = new SonyTV();
		sonyTV1.remoteName = "Remote 1";
		SonyTV sonyTV2 = new SonyTV();
		sonyTV2.remoteName = "Remote 2";
		
		sonyTV1.newMethod();
		//System.out.println(sonyTV1.remoteName + " : " + sonyTV2.remoteName);
	}

}
