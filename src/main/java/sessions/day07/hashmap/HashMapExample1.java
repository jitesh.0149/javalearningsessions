package sessions.day07.hashmap;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import sessions.day06.hashmap.Employee;
import sessions.day06.hashmap.Project;

public class HashMapExample1 {

	public static void main(String[] args) {
		Map<Employee, Project> employeeProjectsMap = new HashMap<Employee, Project>();

		Employee emp1 = new Employee(99, "Rutvik", "Shah");
		Employee emp2 = new Employee(99, "Kunal", "Shah");

		Project proj1 = new Project("1", "BI");
		System.out.println(emp1.hashCode());
		System.out.println(emp2.hashCode());
		
		employeeProjectsMap.put(emp1, proj1);
//		for (Employee e : employeeProjectsMap.keySet()) {
//			System.out.println(e);
//		}
		for (Entry<Employee, Project> m : employeeProjectsMap.entrySet()) {
			System.out.println("Key: " + m.getKey());
			System.out.println("Value: " + m.getValue());
		}

		Project proj2 = new Project("2", "IT");
		System.out.println(emp1.equals(emp2));
		employeeProjectsMap.put(emp2, proj2);
		System.out.println("After Adding new Element ***********");
//		for (Employee e : employeeProjectsMap.keySet()) {
//			System.out.println(e);
//		}
		for (Entry<Employee, Project> m : employeeProjectsMap.entrySet()) {
			System.out.println("Key: " + m.getKey());
			System.out.println("Value: " + m.getValue());
		}

	}

}
