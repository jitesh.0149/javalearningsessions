package sessions.day07.hashmap;

import java.util.HashMap;

public class HashMapExample3 {

	public static void main(String[] args) {
		System.out.println((15 & 1));
		System.out.println((15 & 17));
		System.out.println((15 & 33));
		
		HashMap<Integer, String> hm = new HashMap<Integer, String>();
		hm.put(5, "Test");
		hm.put(1, "Test");
		hm.put(1, "Test1");
		hm.put(17, "Test1");
		hm.put(2, "Test1");

		System.out.println(hm.entrySet());
		hm.put(16, "Test1");
		hm.put(17, "Test1");
		hm.put(18, "Test1");
		hm.put(9, "Test1");
	}

}
