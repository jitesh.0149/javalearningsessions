package sessions.day07.hashmap;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import sessions.day06.hashmap.Employee;
import sessions.day06.hashmap.Project;

import java.util.Set;

public class HashMapExample2 {

	public static void main(String[] args) {
		Map<Employee, Project> employeeProjectsMap = new HashMap<Employee, Project>();

		Employee emp1 = new Employee(99, "Rutvik", "Shah");
		Employee emp2 = new Employee(199, "Kunal", "Shah");
		Project proj1 = new Project("1", "BI");

		employeeProjectsMap.put(emp1, proj1);
		System.out.println(employeeProjectsMap.get(emp1));

		Set<Entry<Employee, Project>> entrySet = employeeProjectsMap.entrySet();
		
		employeeProjectsMap.put(emp2, proj1);
		for(Entry<Employee, Project> s: entrySet) {
			System.out.println(s.getKey());
			System.out.println(s.getValue());
			
		}
		
		System.out.println(employeeProjectsMap.containsKey(new Employee(99,"dd", "dd")));

	}

}
