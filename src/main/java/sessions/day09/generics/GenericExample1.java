package sessions.day09.generics;

//type parameter
public class GenericExample1<P> {
	P manish;
	Integer test;

	public GenericExample1() {
		super();
	}

	public GenericExample1(P p) {
		super();
		this.manish = p;
	}

	public void setP(P p) {
		this.manish = p;
	}

	public void test(P p) {
		System.out.println(p);
	}

	public void test() {
		System.out.println(manish);
	}

}
