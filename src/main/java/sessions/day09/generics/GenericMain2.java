package sessions.day09.generics;

import sessions.day01_2.CanadianHuman;
import sessions.day01_2.IndianHuman;

public class GenericMain2 {

	public static void main(String[] args) {
		GenericExample3<CanadianHuman>	ge = new GenericExample3<>();
		GenericExample3<IndianHuman>	ge1 = new GenericExample3<>();
		ge.test(new CanadianHuman());
		ge1.test(new IndianHuman("test", "Test", "Ttest"));
	}

}
