package sessions.day09.generics;

//type parameter
public class GenericExample2<A, B, C> {
	public void testA(A a) {
		System.out.println(a);
	}

	public void testB(B b) {
		System.out.println(b);
	}

	public void testC(B b) {
		System.out.println(b);
	}
}
