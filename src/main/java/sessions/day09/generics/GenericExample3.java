package sessions.day09.generics;

import sessions.day01_2.Human;

//type parameter
public class GenericExample3<H extends Human> {
	public void test(H h) {
		h.eatFood();
	}
}
