package sessions.day09.generics;

import sessions.day03.interfaces.NetFlixFeatures;

//type parameter
public class GenericExample4<H extends NetFlixFeatures> {
	public void test(H h) {
		h.listOutEpisodes();
	}
	
	public void test2(Object a) {
		System.out.println(a);
	}
}
