package sessions.day09.generics;

import java.util.ArrayList;
import java.util.List;

import sessions.day01_2.CanadianHuman;
import sessions.day03.interfaces.NetFlixFeatures;
import sessions.day03.interfaces.SonyTV;

public class GenericMain3 {

	public static void main(String[] args) {
		GenericExample4<NetFlixFeatures> ge = new GenericExample4<NetFlixFeatures>();

		ge.test(new NetFlixFeatures() {

			@Override
			public void listOutEpisodes() {
				System.out.println("test episodes");

			}
		});
		ge.test(new SonyTV());
		
		
		ge.test2("Test");
		ge.test2(33);
		ge.test2(new CanadianHuman("1", "t", "t"));
	}

}
