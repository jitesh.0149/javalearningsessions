package sessions.day09.generics;

import java.util.ArrayList;
import java.util.HashMap;

import sessions.day01_2.CanadianHuman;

public class GenericMain {

	public static void main(String[] args) {
		GenericExample1<Integer> ge = new GenericExample1<Integer>();
		ge.test(33);
	
		
		String a = "Test";
		System.out.println(a.getClass());
		
		GenericExample1<String> ge1 = new GenericExample1<String>();
		ge1.test("Test2");
		
		
		GenericExample1<CanadianHuman> ge2 = new GenericExample1<CanadianHuman>();
		ge2.test(new CanadianHuman("12345", "testfname", "testlname"));
		
		
//		GenericExample1<String> ge1 = new GenericExample1<String>("Test");
//		ge1.test();

		
		ArrayList<String> list = new ArrayList<String>();
		list.add("Test2");
		System.out.println(list.get(0));
		
		
		ArrayList<CanadianHuman> list1 = new ArrayList<CanadianHuman>();
		list1.add(new CanadianHuman("123", "testfname", "testlname"));
		System.out.println(list1.get(0));
		
	}

}
