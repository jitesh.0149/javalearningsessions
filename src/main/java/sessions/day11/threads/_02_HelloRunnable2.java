package sessions.day11.threads;

import java.util.ArrayList;
import java.util.List;

import sessions.day01_2.CanadianHuman;

public class _02_HelloRunnable2 implements Runnable {

	List<CanadianHuman> listOfCH = new ArrayList<>();

	@Override
	public void run() {
		try {
			Thread.sleep(2000);
			if (Thread.currentThread().getName().equals("Who is top businessman")) {				
				listOfCH.add(new CanadianHuman("2", "Yuvraj", "Rathod"));
			} else if (Thread.currentThread().getName().equals("Who is best car driver")) {
				listOfCH.add(new CanadianHuman("23", "Test", "Driver"));
			} else {
				listOfCH.add(new CanadianHuman("24", "Test", "Player"));
			}

		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void main(String args[]) throws InterruptedException {
		_02_HelloRunnable2 hello = new _02_HelloRunnable2();
		Thread t1 = new Thread(hello, "Who is top businessman");
		Thread t2 = new Thread(hello, "Who is best car driver");
		Thread t3 = new Thread(hello, "Who is best basket ball player");

		t1.start();
		t2.start();
		t3.start();

		t1.join();
		t2.join();
		t3.join();
		System.out.println(hello.listOfCH);
	}
}
