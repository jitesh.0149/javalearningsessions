package sessions.day11.threads;

//Threads & Locks - https://docs.oracle.com/javase/specs/jls/se8/html/jls-17.htexteml#jls-17.4.5-A
public class _01_HelloThread extends Thread {

	@Override
	public void run() {
		// This is database or an I/O operation

		try {
			System.out.println("Started ..." + Thread.currentThread().getName());
			Thread.sleep(5000); // for having a pause 1*1000 = millisecond , 1*1000000
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println("Completed");
	}

	public static void main(String args[]) throws InterruptedException {
		_01_HelloThread t1 = new _01_HelloThread(); // Get Loaded Status
		_01_HelloThread t2 = new _01_HelloThread(); // Get Other Status
		_01_HelloThread t3 = new _01_HelloThread();
		System.out.println("Main Thread ..." + Thread.currentThread().getName());
		t1.start();
		t2.start();
		t3.start();
		System.out.println("Is t1 alive ?"+t1.isAlive());
		t1.join(); // 2 sec
		t2.join(); // 5 sec
		t3.join(); // 3 sec
		//Combine and show it to user data for all the status
		System.out.println("My program is completed .....");

	}
}
