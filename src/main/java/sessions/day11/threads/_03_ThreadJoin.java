package sessions.day11.threads;

public class _03_ThreadJoin implements Runnable {
	long waitingTime;

	public _03_ThreadJoin(long waitingTime) {
		this.waitingTime = waitingTime;
	}

	public static void main(String[] args) throws InterruptedException {
		Thread t1 = new Thread(new _03_ThreadJoin(4000));
		Thread t2 = new Thread(new _03_ThreadJoin(20));
		t1.start();
		t2.start();		
	
		System.out.println(t1.getName() + " is Alive? : " + t1.isAlive());
		t1.join(); // Will wait till t1 is completed
		System.out.println(t1.getName() + " is completed");
		System.out.println(t2.getName() + " is Alive? : " + t2.isAlive());
		t2.join();
		System.out.println(t2.getName() + " is completed");
		
		/*
		 * Below operations are deprecated
		 * t1.stop(); t1.resume(); t1.suspend() t1.countStackFrames();
		 * https://docs.oracle.com/javase/8/docs/technotes/guides/concurrency/threadPrimitiveDeprecation.html
		 */
	}

	@Override
	public void run() {
		try {
			System.out.println("Waiting time for " + Thread.currentThread().getName()
					+ " : " + waitingTime);
			Thread.sleep(waitingTime);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
