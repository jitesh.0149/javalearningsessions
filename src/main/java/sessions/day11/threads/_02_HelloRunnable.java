package sessions.day11.threads;

public class _02_HelloRunnable implements Runnable {
	String fname;

	@Override
	public void run() {
		System.out.println("Hello from a thread!" + this.fname+" :Name: "+Thread.currentThread().getName());
	}

	public static void main(String args[]) {
		_02_HelloRunnable hello = new _02_HelloRunnable();
		hello.fname = "Test";
		Thread t1 = new Thread(hello,"My First THread");
		t1.start();
		
		
		_02_HelloRunnable hello1 = new _02_HelloRunnable();
		hello1.fname = "Test1";
		Thread t2 = new Thread(hello1);
		t2.start();
	}
}
